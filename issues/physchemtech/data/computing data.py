
# coding: utf-8

# In[14]:


import matplotlib.pyplot as plt
from numpy import linspace, sqrt
import pandas as pd
import csv
get_ipython().run_line_magic('matplotlib', 'inline')


# In[6]:


x = 0.2
d = 8e-3
gamma_p = 8.91e3
gamma_f = 0.26055
S = 0.05
mu = 0.82e-4


# ----------

# In[7]:


start, stop, step = 1000, 2000, 50
U_f = list(range(start, stop + step, step))


# In[8]:


Re = list(map(lambda U_f: (gamma_f * U_f * S) / mu, U_f))


# In[9]:


c_d = list(map(lambda Re: 24 / Re, Re))


# In[11]:


A = map(lambda c_x: 3/4 * gamma_f/gamma_p * c_d/d, c_d)


# In[12]:


def U_p(U_f, c_d, x=0.2, d=8e-3):
    return U_f * sqrt(3/2 * gamma_f/gamma_p * c_d/d * x)


# In[15]:


with open('up_d_.csv', 'w', newline='') as csvfile:
    fieldnames = ['Uf', 'd', 'Re', 'Cd', 'Up']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    
    for U_f in range(1000, 2050, 50):
        Re = (gamma_f * S * U_f) / mu
        c_d = 24 / Re
        for d in linspace(0.1, 0.5, 100):
            U_p = U_f * sqrt((3/2) * (gamma_f / gamma_f) * (c_d/d) * 0.2)
            writer.writerow({'Uf': U_f, 'd': d, 'Re': Re, 'Cd': c_d, 'Up': U_p})

