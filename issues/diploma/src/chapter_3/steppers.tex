\subsection{Управление шаговыми двигателями} % (fold)
\label{sub:управление_шаговыми_двигателями}

Работа предлагаемого линейного контроллера скорости основана на алгоритме, описанном в статье Austin \cite{Austin:2004}. 
Этот алгоритм допускает параметризацию и расчет в реальном времени, используя только простые арифметические операции над числами с фиксированной точкой, без применения таблиц данных.

\paragraph{Основные уравнения шагового двигателя} % (fold)
\label{par:основные_уравнения_шагового_двигателя}

Для создания вращательного движения в шаговом двигателе должен соблюдаться правильный порядок изменения тока в обмотках.
Это достигается использованием драйвера, который при подаче на него импульсного сигнала (импульс шагового двигателя) и сигнала направления создает корректную выходную последовательность.

Для вращения шагового двигателя с постоянной скоростью импульсы должны генерироваться с установившейся частотой. Счетчик генерирует эти импульсы, работая на частоте $f_t, \si{\hertz}$. Задержка $\delta t$, программируемая счетчиком $c$:
\begin{equation}
	\delta t = \frac{c}{f_t},~[\si{\second}].
\end{equation}

Шаговый угол двигателя $\alpha$, угол поворота вала $\theta$ и скорость $\omega$ задаются уравнениями:
\begin{equation}
	\alpha = \frac{2\pi}{spr},~[\si{\radian}]; \quad \theta = n \alpha,~[\si{\radian}]; \quad \omega = \frac{\alpha}{\delta t},~[\si{\radian\per\second}],
\end{equation}
где $spr$ -- число шагов на один оборот, $n$ -- число шагов, а $\SI{1}{\radian\per\second} = \SI{9.55}{\text{об}\per\minute}$.

% paragraph основные_уравнения_шагового_двигателя (end)

\paragraph{Линейное изменение скорости} % (fold)
\label{par:линейное_изменение_скорости}

Для мягкого старта и остановки двигателя необходим контроль ускорения и замедления. 
На рисунке \ref{fig:figure1} показана взаимосвязь между ускорением, скоростью и положением. 
Использование постоянного ускорения / замедления дает линейный профиль скорости.

\begin{figure}[tb]
	\centering
	\input{img/stepper_accel_speed_angle}
	\caption{Зависимость ускорения $\dot\omega$, скорости $\omega$ и угла вала $\theta$ от времени $t$}
	\label{fig:figure1}
\end{figure}

Временная задержка $\delta t$ между импульсами шагового двигателя контролирует скорость.
Эта задержка должна рассчитываться с целью обеспечить насколько это возможно более близкое следование скорости шагового двигателя заданному линейному изменению скорости.


Дискретные шаги контролируют движение шагового двигателя, а разрешение временной задержки между этими шагами задается частотой таймера.
% paragraph линейное_изменение_скорости (end)

\subparagraph{Точный расчет задержки между шагами} % (fold)
\label{subp:точный_расчет_задержки_между_шагами}

Первая задержка счетчика $c_0$, а также последующие задержки $c_n$ определяются уравнениями:
\begin{equation}
	c_0 = \frac{1}{t_t} \sqrt{\frac{2 \alpha}{\dot\omega}}; \quad c_n = c_0 \qty(\sqrt{n+1} - \sqrt{n}).
\end{equation}

Вычислительная мощность микроконтроллера ограничена, а расчет двух квадратных корней -- трудоемкий процесс. 
Поэтому рассматривается аппроксимация с меньшей вычислительной сложностью.

Значение счетчика в момент времени $n$ для интервала времени между шагами рассчитывается с использованием аппроксимации рядами Тейлора по формуле:
\begin{equation}
	c_n = c_{n-1} - \frac{2 c_{n-1}}{4n + 1}.
\end{equation}

Это вычисление осуществляется намного быстрее, чем вычисление двух квадратных корней, но при $n=1$ вносит ошибку величиной \num{0.44}.
Способом компенсации этой ошибки является умножение $c_0$ на \num{0.676}.

% subparagraph точный_расчет_задержки_между_шагами (end)

\subparagraph{Изменение (рампа) ускорения} % (fold)
\label{subp:изменение_рампа_ускорения}

Ускорение задается значениями $c_0$ и $n$. 
Если выполняется изменение в ускорении (или замедлении), должен быть выполнен расчет нового значения $n$.

Время $t_n$ и $n$ как функции от ускорения двигателя, скорости и шагового угла задаются уравнениями:
\begin{equation}
	t_n = \frac{\omega_n}{\dot\omega}; \quad n = \frac{\dot\omega t_n^2}{2 \alpha}.
\end{equation}

Объединение этих уравнений дает соотношение:
\begin{equation}
	n \dot\omega = \frac{\omega^2}{2 \alpha}.
\end{equation}

Из него видно, что число шагов, необходимое для достижения заданной скорости, обратно пропорционально ускорению:
\begin{equation}
	n_1 \dot\omega_1 = n_2 \dot\omega_2.
\end{equation}

Это означает, что изменение ускорения от $\dot\omega_1$ до $\dot\omega_2$ осуществляется изменением $n$.

При перемещении на заданное число шагов для достижения нулевой скорости, замедление необходимо начать на правильном шаге.
Следующее уравнение используется для нахождения $n_1$:
\begin{equation}
	n_1 = \frac{(n_2 + n_1) \dot\omega_2}{\dot\omega_1 + \dot\omega_2}.
\end{equation}
% subparagraph изменение_рампа_ускорения (end)

\paragraph{Программная реализация} % (fold)
\label{par:программная_реализация}

Как упоминалось в п.\ref{ssub:контроллер_шагового_двигателя} для управления вращением шагового двигателя с помощью драйвера при заданном режиме микрошага используются два сигнала -- \texttt{DIR} для задания направления движения и \texttt{STEP} для выполнения непосредственно шага.

% 3 timers (TIM1, TIM2 and TIM3) configured to run in PWM mode using timer output channels (so the stepping pulse pin signalled automatically by TIM pheripheral).
Для управления сигналами STEP двух драйверов используются аппаратные счетчики (таймерами) TIM1 и TIM2, работающие в ШИМ-режиме.
Таким образом, шаговый сигнал генерируется автоматически аппаратным блоком таймеров.

Драйвер оси Y управляется таймером \texttt{TIM1}, выход \texttt{PA10}; направление задает GPIO выход \texttt{PB6}.
Драйвер оси Z управляется таймером \texttt{TIM2}, выход \texttt{PB3}; направление задает GPIO выход \texttt{PC11}.

%     X axis - TIM1, PWM stepping pulses at PA10, Direction GPIO out at PB4
%     Y axis - TIM2, PWM stepping pulses at PB3, Direction GPIO out at PB10
%     Z axis - TIM3, PWM stepping pulses at PB5, Direction GPIO out at PA8

% The TIM_UPDATE interrupt handler is also enabled for each timer.
Также для каждого таймера включено системное прерывание \texttt{TIM\_UPDATE}.
% It is used to count PWM steps pulses. 
Оно используется для подсчета шагов ШИМ и имеет самый высокий приоритет относительно других.
% These iterrupts configured with highest priority to others. 
% So we don't miss the steps count and can easily run all three motros at 400kHz. 
Таким образом исключается пропуск шагов и появляется возможность запускать каждый двигатель на частоте до \SI{400}{\kilo\hertz}.
% Step pulse pin however is not flipped in interrupt handler programatically (as been said - this happens through PWM mode).
Однако импульсный вывод \texttt{STEP} не обрабатывается программно обработчиком прерываний (как уже было сказано -- это происходит в ШИМ-режиме).
% PWM guarantees uniform pulsing, while interrupt handler routine is always a bit delayed and the delay duration varies every time (not much, tens to hundreds of nanoseconds, but at high speed this is critical).
ШИМ гарантирует равномерную пульсацию, в то время как процедура обработки прерывания всегда немного задерживается, и длительность этой задержки каждый раз меняется (от десятков до сотен наносекунд, что становится очень важно на высокой скорости).

% There is one more timer configured - TIM14. 
Также в системе включен еще один таймер -- \texttt{TIM16}.
% It runs in a regular mode, simply excuting its TIM_UPDATE interrupt routine every 50 microseconds. 
Он включается в обычном режиме для выполнения своего прерывания \texttt{TIM\_UPDATE} каждые \SI{50}{\micro\second}.
% This is a stepper controller timer, it checks the current speed of each connected mottor, estimates the time left to reach the destination (target step number) and comperas it with the time required to reduce the speed to the minimum (starting/stopping step time). 
Этот таймер используется для проверки текущей скорости шаговых двигателей, расчета времени, остающегося до достижения целевого положения, сравнения этого времени с расчетным для управления ускорением/торможением.
% And changes the speed accodringly (accellerating/decelerating the motor, or just keeping it at maximum allowed speed).

Информация о шаговом двигателе хранится в структуре \texttt{stepper\_state}, описанной в листинге \ref{src:stepper_state}. 
Она содержит в себе условное обозначение ШД, ссылки на управляющие порты, информацию о минимальной, максимальной и текущей скоростях и ускорениях, текущем и целевом положении, а также о состоянии двигателя.
\lstinputlisting[
	caption={Структура информации о параметрах ШД}, 
	label={src:stepper_state},
	language=c,
	firstline=32, lastline=48
]{inc/Inc/stepperController.h}

Возможные состояния двигателя описаны в структуре \texttt{stepper\_status} в листинге \ref{src:stepper_status}, и определяют следующие события: состояние двигателя неизвестно; двигатель вращается ``вперед''; двигатель вращается ``назад''; двигатель стартует; двигатель останавливается; двигатель остановлен.
\lstinputlisting[
	caption={Возможные состояния ШД}, 
	label={src:stepper_status},
	language=c, 
	firstline=14, lastline=22
]{inc/Inc/stepperController.h}

% paragraph программная_реализация (end)
% subsection управление_шаговыми_двигателями (end)