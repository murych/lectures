\subsection{Процессы с участием нуклеиновых кислот} % (fold)
\label{sub:процессы_с_участием_нуклеиновых_кислот}

Основная функция нуклеиновых кислот заключается в хранении, воспроизведении и передаче генетической информации. 
Являясь системами динамическими, нуклеиновые кислоты осуществляют все процессы с высокой скоростью и эффективностью, постоянно взаимодействуя с соответствующими белками, прежде всего с ферментами.
Главными процессами с их участием являются \emph{репликация}, \emph{транскрипция} и \emph{трансляция}.

\subsubsection{Репликация} % (fold)
\label{ssub:репликация}

Репликацией называется процесс удвоения ДНК, его механизм вытекает из строения двуспиральной молекулы ДНК. 
В результате репликации образуются две новые молекулы ДНК, представляющие собой точные копии исходной молекулы.
Каждая из новых молекул содержит одну цепь исходной ДНК и одну заново синтезированную цепь, т.е. половина родительской молекулы сохраняется в дочерней.

Однако даже в самых простых случаях этот механизм сочетается из множества сложных процессов, в которые вовлечены многочисленные ферменты и регуляторные белки.

Лучше всего процессы репликации изучены для наиболее простых систем -- бактерий, бактериофагов и внехромосомных генетических элементов бактерий -- плазмид.

Принято использовать понятие ``репликон'', предложенное в 1963 г. Ф. Жакобом, С. Бреннером и Ф. Кьюзеном для обозначения генетической единицы репликации, т. е. сегмента ДНК, который автономно воспроизводится (реплицируется) в процессе клеточного роста и деления. 
Каждый репликон должен иметь систему ``управления'' собственной репликацией. 
Хромосомы \emph{Е. coli}, плазмиды, ДНК бактериофагов представляют собой репликоны разной сложности, способные к автономной репликации в клетке и имеющие систему инициации. 
Репликон может содержать в себе гены, кодирующие синтез всех белков, необходимых для репликации (хромосома \emph{Е. coli}), части таких белков (некоторые сравнительно крупные бактериофаги) или использовать для своей репликации практически только чужие белки (мелкие фаги М13 или G-4, содержащие однонитевые циклические ДНК) \cite{Овчинников:1987}.

Ключевую роль в процессе репликации играют реплицирующие ДНК-полимеразы, которые осуществляют матричный синтез ДНК из дезоксинуклеозидтрифосфатов.
Растущая цепь удлиняется на одно звено, и процесс повторяется с новым дезоксинуклеозидтрифосфатом. 
Для того чтобы ДНК-полимераза могла начать синтез, необходимо существование уже готового фрагмента ДНК или РНК, комплементарного матрице и содержащего свободную \(3'\)-\ce{OH}-группу. 
Этот фрагмент называют \emph{затравкой}. 
В процессе синтеза дочерних цепей родительская двухцепочечная ДНК расплетается, образуя структуру, по форме напоминающую латинскую букву Y.
Такая структура называется репликативной вилкой \cite{Овчинников:1987}.

% subsection репликация (end)

\subsubsection{Транскрипция} % (fold)
\label{ssub:транскрипция}

Транскрипцией называется процесс биосинтеза РНК в клетке.
Его результатом является образование РНК, комплементарных отдельным участкам ДНК. 
При этом в каждом участке РНК комплементарна только одной определенной нити ДНК.

Процесс биосинтеза РНК осуществляется ферментами -- РНК-полимеразами, которые используют ДНК в качестве матрицы. 
Как и в случае репликации, механизм образования фосфодиэфирных связей включает в себя катализируемую ферментом нуклеофильную атаку \(3'\)-гидроксильной группы растущей цепи на а-фосфатную группу присоединяемого субстрата (рибонуклеозидтрифосфата).
При образовании фосфодиэфирной связи от трифосфата отщепляется неорганический пирофосфат. 
Каждый вновь присоединяемый нуклеозид комплементарен тому звену матрицы, которое является ближайшим \(5'\)-соседом только что скопированного звена. 
Цепь РНК растет в направлении \(5' \rightarrow 3'\) по мере движения РНК-полимеразы по копируемой цепи ДНК в направлении от \(3'\)-конца к \(5'\)-концу \cite{Овчинников:1987}.
Схема процесса транскрипции показана на рисунке \ref{fig:03_01}.

\begin{figure}[tb]
	\centering
	\includegraphics[width=.8\linewidth]{img/2019-04-10_02-42-04.png}
	\caption{Последовательное копирование одной из цепей ДНК в процессе транскрипции~\protect\cite{Овчинников:1987}}
	\label{fig:03_01}
\end{figure}

Как и репликация, транскрипция состоит из трех основных этапов: \emph{инициации}, \emph{элонгации} и \emph{терминации}. 
В отличие от ДНК-полимераз, РНК-полимеразы способны к самостоятельной инициации синтеза РНК, которая осуществляется в определенных точках ДНК. 
Место инициации синтеза РНК определяется специальными регуляторными участками ДНК -- промоторами. 
Терминация синтеза также происходит на специфических участках ДНК -- терминаторах. 
Процесс транскрипции регулируется разнообразными способами, что позволяет клетке приспосабливаться к изменениям условий существования. 
Наиболее хорошо изучены транскрипция и способы ее регуляции у бактерий и бактериофагов.

% subsection транскрипция (end)

\subsubsection{Трансляция} % (fold)
\label{ssub:трансляция}

Важным этапом на пути экспрессии гена является трансляция синтезированной мРНК. 
Трансляция -- сложнейший многоступенчатый процесс синтеза полипептидной цепи согласно информации, заключенной в последовательности нуклеотидов мРНК.
Трансляция осуществляется на рибосоме, в нее вовлечены также белковые факторы, GTP и аминоацил-тРНК -- молекулы тРНК, несущие активированные аминокислоты. 
Как и другие процессы матричного синтеза, процесс трансляции условно делят на три стадии: \emph{инициация}, \emph{элонгация} и \emph{терминация}.

При инициации происходит специфическое связывание рибосомы с мРНК и с первой аминоацил-тРНК, называемой инициаторной, в результате чего образуется комплекс, способный к синтезу белка -- инициаторный комплекс. 
При элонгации осуществляется последовательное связывание аминоацил-тРНК с образованием пептидных связей по программе, задаваемой последовательностью кодонов в мРНК. 
Терминация представляет собой отщепление готовой белковой цепи от трансляционного комплекса.
Все этапы синтеза белка осуществляются при участии специальных белковых факторов, которые называются соответственно факторами инициации IF, элонгации -- EF и терминации RF.
Энергия для процесса трансляции черпается при гидролизе GTP.

В ходе трансляции нуклеотидная последовательность мРНК считывается в направлении от \(5'\)- к \(3'\)-концу.
Считывание происходит по законам генетического кода, согласно которым каждой аминокислоте соответствует триплет нуклеотидов (кодон), каждый кодон кодирует только одну аминокислоту, а последовательность кодонов в мРНК определяет аминокислотную последовательность синтезируемого белка.
Функцию узнавания кодона осуществляет не сама аминокислота, а молекула тРНК, к которой она присоединена и которая содержит последовательность нуклеотидов, комплементарную кодону — антикодон.
Иными словами, тРНК служит адаптором, обеспечивающим соответствие между кодоном и кодируемой им аминокислотой. 
Наличие адаптора -- основное отличие трансляции, как процесса матричного синтеза, от транскрипции и репликации, в которых продукт имеет ту же химическую природу, что и матрица.

В клетке реализуется и другая ситуация, когда для одной аминокислоты существует несколько специфичных тРНК. 
Такие тРНК называются изоакцепторными.

% subsection трансляция (end)

\subsubsection{Полимеразная цепная реакция} % (fold)
\label{ssub:полимеразная_цепная_реакция}

% В последние годы все больше молекулярно-биологических методов находят практическое применение в различных областях медицины, промышленности и сельского хозяйства. 
% \red{Полимеразная цепная реакция (ПЦР) -- метод, позволяющий нарабатывать в пробирке определенный участок молекулы дезоксирибонуклеиновой кислоты (ДНК) практически в неограниченных количествах.}
ПЦР -- это метод, заключающийся в том, что в пробирке нарабатывается конкретный диапазон молекулы дезоксирибонуклеиновой кислоты (ДНК) в почти неограниченном объеме.

% \red{В медицине ПЦР применяют при диагностике инфекционных и наследственных заболеваний, при диагностике рака и иммунных патологий. 
На практике ПЦР применимо в случаях таких заболеваний, как различные инфекционные, наследственные, раковые и иммунные.
% ПЦР используют для идентификации личности и определения биологического родства индивидов.
ПЦР применяется при необходимости идентифицировать человека или определить биологическое родство.
% Санитарно-эпидемиологические службы используют ПЦР для контроля за микробиологическим загрязнением окружающей среды и продуктов питания, а также для выявления генетически модифицированных источников пищи (ГМИ). 
Санитарно-эпидемиологические службы применяют метод ПЦР с целью контроля микробиологического загрязнения окружающей среды и продуктов питания.
% В научно-исследовательских лабораториях ПЦР используют для изучения нуклеиновых кислот и проведения манипуляций с ними.
В НИИ метод ПЦР применяют для анализа нуклеиновы кислот и выполнения с ними различных операций.
% Например, благодаря ПЦР стало возможным быстрое получение исследуемых участков ДНК в чистом виде и в достаточном количестве.}
Так, благодаря ПЦР ученые получили возможность быстрого формирования анализируемых участков ДНК в чистом виде и в достаточном количестве.

% выдержка из ПЦР В РЕАЛЬНОМ ВРЕМЕНИ
% \red{ПЦР представляет собой метод ферментативной наработки \emph{in vitro} определенных, сравнительно коротких (от нескольких десятков до нескольких тысяч пар нуклеотидов), двуцепочечных фрагментов ДНК.
ПЦР -- это метод ферментативной наработки in vitro конкретных, достаточно коротких (несколько десятков -- несколько тыся пар нуклеотдиов), двухцепочечных фрагментов ДНК.
% В основе реакции лежит механизм, который в природе реализован привнутриклеточном удвоении (репликаций) молекул ДНК ферментом ДНК-полимеразой. 
Базисом для данной реакции является механизм, реализующийся в природе при внутриклеточном удвоении -- репликации -- молекул ДНК ферментом ДНК-полимеразой.
% Для протекания этой реакции (в клетке или пробирке) необходимы следующие ключевые компоненты: 
В целях успешного протекания этой реакции (в клетке или в пробирке) необходим следующие компоненты:
\begin{itemize}
	\item исходная молекула ДНК (служащая матрицей для репликации);
	\item фермент ДНК-зависимая-ДНК-полимераза;
	\item дезоксирибонуклеотид три фосфаты;
	\item короткие одноцепочечные ДНҚ-затравки (праймеры), комплементарные матричной ДНК.
\end{itemize}

% \red{Если все перечисленные компоненты смешать в соответствующем солевом растворе (буфере), ДНК-полимераза будет синтезировать ДНК из дезоксирибонуклеозидтрифосфаты, подставляя их по принципу комплементарности к матричной молекуле ДНК начиная от ДНКзатравки.}
При смешении перечисленных компонентов в соответствующем солевом растворе -- буфере -- ДНК-полимераза будет синтезировать ДНК, подставляя дезоксирибонуклеозидтрифосфаты по принципу комплиментарности к матричной молекуле ДНК.

% \red{При проведении ПЦР используют, как правило, два праймера, взаимодействующих (гибридизующихся) в соответствии с принципом комплементарности с противоположными цепями ДНК и ограничивающих участок матричной молекулы, который и будет амплифицирован в ходе реакции. 
При использовании метода ПЦР применяют, в основном, два взаимодействующих по принципу комплементарности праймера, которые имеют противоположные цепи ДНК.
Они ограничивают участок матричной молекулы, амплифицирующийся в течение реакции.
% Поскольку для гибридизации праймеров необходимо предварительно разъединить две цепи матричной молекулы ДНК, реакционную смесь нагревают до \SIrange{93}{96}{\celsius}.
Так как для успешной гибридизации праймеров необходимо заранее разъединить две цепи матричной молекулы ДНК, реакционную смесь необходимо предварительно нагреть до \SIrange{93}{96}{\celsius}.
% Затем смесь охлаждают до температуры, при которой праймеры могут провзаимодействовать с одноцепочечной матричной ДНК (\SIrange{40}{75}{\celsius}).
После этого полученную смесь охлаждают до температуры, при которой праймеры начинают взаимодействовать с одноцепочной матричной ДНК -- \SIrange{40}{75}{\celsius}.
% После взаимодействия праймеров сматричной молекулой ДНК, полимераза синтезирует комплементарную цепь ДНК путем удлинения праймеров (при температуре \SIrange{60}{75}{\celsius}).}
После того, как праймеры осуществили взаимодействие осуществили взаимодействие с одноцепочной матричной ДНК, полимераза начинает синтез комплементарной цепи ДНК через удлинение праймеров при температуре \SIrange{60}{75}{\celsius}.

% \red{Если повторить нагрев и охлаждение реакционной смеси (цикл реакции), то ранее синтезированные молекулы ДНК выступят в качестве матриц для синтеза новых молекул, что приведет к увеличению ограниченного праймерами фрагмента вдвое.
При повторении нагрева и охлаждения реакционной смеси, ранее синтезированные молекулы ДНК начнут выступать в роли матриц для формирования новых молекул.
Это приведет к удвоению ограниченного праймерами фрагмента.
% Многократное повторение таких температурных циклов ведет к увеличению количества ограниченного праймерами участка ДНК в геометрической прогрессии с основанием, близким к 2.
Многократное повторение этих температурных циклов приводит к росту ограниченного праймерами участка ДНК в геометрической прогрессии с основанием, близким к двойке.
Схематическое представление амплификации дано на рис. \ref{fig:03_03_принцип_экспоненциальной_амплификации}.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.9\linewidth]{img/2019-04-10_03-02-01.png}
	\caption{Принцип экспоненциальной амплификации ДНК в ходе ПЦР~\protect\cite{Ребриков:2015}}
	\label{fig:03_03_принцип_экспоненциальной_амплификации}
\end{figure}

Таким образом, классическая ПЦР состоит из повторяющихся температурных циклов, состоящих, в свою очередь, из трех температурных режимов: 
\begin{enumerate}
	\item разрушение водородных связей между цепями ДНК (\SIrange{93}{96}{\celsius});
	\item гибридизация праймеров на днк (\SIrange{40}{75}{\celsius});
	\item синтез комплементарных цепей ДНК путем удлинения праймеров (\SIrange{60}{75}{\celsius}).
\end{enumerate}

% \red{В результате повторения циклов ПЦР, увеличение количества ограниченного праймерами фрагмента ДНК идет в геометрической прогрессии, поскольку ранее синтезированные фрагменты на каждом цикле реакции выступают в качестве матриц для синтеза новых фрагментов.
% Как правило, для получения достаточного для детекции количества ДНК, в зависимости от начальной концентрации матриц и эффективности реакции, необходимо от 20 до 50 циклов ПЦР (считается, что даже с единственной стартовой молекуклы за 40 циклов высокоэффективной ПЦР можно получить достаточное для детекции количество продукта реакции) \cite{Ребриков:2015}. 
В основном, в целях получения достаточного для детекции количества ДНК, в зависимости от начальной концентрации матриц и эффективности реакции, проводится от 20 до 50 циклов ПЦР.
Считается, что даже с единственной стартовой молекулы за 40 циклов высокоэффективной ПЦР можно получить достаточное для детекции количество продукта реакции \cite{Ребриков:2015}.
% В настоящее время реакцию проводят в специальных программруемых термостатах, автоматически меняющих температуру реакционной смеси по заданной программе.}
В современных лабораториях данная реакция проводится в специализированных программируемых термостатах, которые автоматически изменяют температуру реакционной смеси согласно заранее заданной программе.
% конец выдержки

\paragraph{История развития} % (fold)
\label{par:история_развития}

В 1955 г. А. Корнберг открыл фермент, который назвал ДНК-полимеразой \cite{Овчинников:1987}. 
Этот фермент способен удлинять цепь ДНК, присоединяя нуклеотиды к ее $3'$-концу.
В искусственных условиях фермент катализирует реакцию достраивания участка искомой ДНК от затравки (праймера), которая комплементарно связана с цепью ДНК (матрицей). 
Раствор, в котором происходит эта реакция, должен содержать нуклеозидтрифосфаты (дНТФ), используемые в качестве строительных блоков.

В 1971 г. Х. Клеппе и соавторы рассмотрели данные, касающиеся состава ингредиентов реакционной смеси, и принципы использования коротких искусственно синтезированных молекул ДНК-праймеров для получения новых копий ДНК \cite{Овчинников:1987}.

Однако возможность использования ПЦР в плане наработки большого количества копий нуклеиновых кислот еще не рассматривалась. 
Это было связано с техническими трудностями, обусловленными необходимостью трудоемкого синтеза праймеров, и нестабильностью фермента. 
В начале использования метода ПЦР после каждого цикла нагревания и охлаждения ДНК-полимеразу приходилось добавлять в реакционную смесь, так как она быстро инактивировалась при высокой температуре, необходимой для разделения цепей спирали ДНК. 
Процедура была очень неэффективной, требовала много времени и фермента.

В 1975 г. Т. Брок и Х. Фриз открыли \emph{Thermus aquaticus} -- грамотрицательную палочковидную экстремально термофильную бактерию, а в 1976 г. из нее была впервые выделена Taq-полимераза \cite{Овчинников:1987}. 
Преимуществом данного фермента была способность стабильно работать при повышенных температурах (оптимум \SIrange{72}{80}{\celsius}).

В 1983-1984 гг. К. Мюллис провел ряд экспериментов по разработке ПЦР и первым начал использовать Taq-полимеразу вместо неустойчивой к высоким температурам ДНК-полимеразы \cite{Овчинников:1987, Mullis:1993}. 
Это позволило ускорить работы по разработке полимеразной цепной реакции. 
Кроме того, К. Мюллис вместе с Ф. Фалуном разработали алгоритм циклических изменений температуры в ходе ПЦР.

% \red{Таким образом, сформировался принцип использования ПЦР как метода амплификации \emph{in vitro} заданных фрагментов ДНК с полностью или частично известной последовательностью.
В следствие этого возник принцип использования ПЦР как метода амплификации in vitro заданных фрагментов ДНК с полностью или частично известной последовательностью \cite{Овчинников:1987}.
% Результатом открытия ПЦР стало почти немедленное практическое применение метода.}
Метод практически сразу после открытия стал активно применяться на практике.

% \red{В 1985 г. Саики с соавторами опубликовали статью, в которой была описана амплификация геномной последовательности $\beta$-глобина \cite{Овчинников:1987}. 
В 1985 году была опубликована статья под авторством Саики с соавторами, в котрой подробно описывался механизм амплификации геномной последовательности $\beta$-глобина \cite{Овчинников:1987}.
% С этого момента количество публикаций о применении ПЦР в своей работе стало увеличиваться в геометрической прогрессии.
Начиная с той публикации наблюдался резкий рост количества сценариев исследований практического использования ПЦР.
% Особенно бурное развитие метод ПЦР получил благодаря международной программе ``Геном человека'' \cite{HumanGenome:1990}. 
Одним из ключевых факторов такого скачка явилась международная программа ``Геном человека'' \cite{HumanGenome:1990}.
% Были созданы современные лазерные технологии секвенирования (расшифровки нуклеотидных последовательностей ДНК).
В этот период были разработаны современные лазерные технологии секвенирования (расшифровки нуклеотидных последовательностей ДНК).
% Это в свою очередь способствовало значительному росту информационных баз данных, содержащих последовательности ДНК биологических объектов.
Этот фактор позволил обеспечить колоссальный рост информационных баз данных, которые содержат множество последовательностей ДНК биологических объектов.
% В настоящее время предложены различные модификации ПЦР, показана возможность создания тест-систем для обнаружения микроорганизмов, выявления точечных мутаций, описаны десятки различных применений метода.
% Таким образом, открытие метода ПЦР стало одним из наиболее выдающихся событий в области молекулярной биологии за последние десятилетия.
Открытие метода ПЦР стало одним из наиболее выдающихся научных открытий в сфере молекулярной биологии за последние десятилетия.
% Это позволило поднять медицинскую диагностику на качественно новый уровень.}
Этот метод поднял медицинскую диагностику на следующий этап развития.

% paragraph история_развития (end)

\paragraph{Основные компоненты} % (fold)
\label{par:основные_компоненты}

Для проведения ПЦР необходимо наличие в реакционной смеси (РС) ряда основных компонентов.

\emph{Праймеры} -- искусственно синтезированные олигонуклеотиды, имеющие, как правило, размер от 15 до 30 нуклеотидов, идентичные (комплементарные) противоположным концам противоположных цепей искомого участка ДНК-мишени \cite{Зорина:2012}. 
Служат затравкой для синтеза комплементарной цепи с помощью ДНК-полимеразы и играют ключевую роль в образовании и накоплении продуктов реакции амплификации.
Правильно подобранные праймеры обеспечивают специфичность и чувствительность тест-системы и должны отвечать ряду критериев \cite{Зорина:2012}:
\begin{itemize}
	\item быть специфичными. Особое внимание уделяют $3'$-концам праймеров, так как именно с них Taq-полимераза начинает достраивать комплементарную цепь ДНК. При недостаточной специфичности праймеров в процессе ПЦР будут образовываться продукты, которые, с одной стороны, могут быть идентифицированы как ложноположительный результат, а с другой стороны, на процессы их накопления будут расходоваться компоненты реакционной смеси, что приведет к значительной потере чувствительности реакции как таковой;
	\item не должны образовывать димеры и петли -- устойчивые двойные цепи -- при отжиге праймеров самих на себя или друг с другом;
	\item область отжига праймеров должна находиться вне зон мутаций, делеций или инсерций в пределах видовой или иной специфичности, взятой в качестве критерия при выборе праймеров. При попадании в такую зону не происходит отжига праймеров и, как следствие, возникает ложноотрицательный результат.
\end{itemize}

\emph{Taq-полимераза} -- термостабильный фермент, который обеспечивает достраивание $3'$-конца второй цепи ДНК согласно принципу комплементарности \cite{Зорина:2012}.

\emph{Смесь дезоксинуклеотидтрифосфатов} (дНТФ) -- дезоксиаденозинтрифосфат (дАТФ), дезоксигуанозинтрифосфат (дГТФ), дезоксицитозинтрифосфат (дЦТФ) и дезокситимидинтрифосфат (дТТФ) – строительный материал, используемый Taq-полимеразой для синтеза второй цепи ДНК \cite{Зорина:2012}.

\emph{Буфер} -- смесь катионов и анионов в определенной концентрации, обеспечивающей оптимальные условия для реакции, а также стабильное значение рH \cite{Зорина:2012}.

\emph{Анализируемый образец} -- подготовленный к внесению в реакционную смесь препарат, который может содержать искомую НК, например ДНК микроорганизмов, служащую мишенью для последующего многократного копирования \cite{Зорина:2012}. 
При отсутствии НК-мишени специфический продукт амплификации не образуется.

% paragraph основные_компоненты (end)

% \paragraph{Дополнительные компоненты} % (fold)
% \label{par:дополнительные_компоненты}
% % \red{Для удобства детекции и контроля эффективности амплификации в состав реакционной смеси могут быть включены дополнительные компоненты.}
% В состав реакционной смеси могут быть включены дополнительные компоненты для повышения качества детекции и контроля эффективности амплификации.

% \subparagraph{Положительный контроль} % (fold)
% \label{par:положительный_контроль}
% Положительный контрольный образец (ПКО) представляет собой искусственно синтезированную олигонкулеотидную последовательность, строго соответствующую искомой \cite{Зорина:2012}. 
% Соответственно, праймеры для ПКО и искомой мишени одинаковые, что позволяет удостовериться в работоспособности и сохранности компонентов РС, необходимых для нормального прохождения ПЦР.
% % subparagraph положительный_контроль (end)

% \subparagraph{Отрицательный контроль} % (fold)
% \label{par:отрицательный_контроль}
% Отрицательный контрольный образец (ОКО) включает в себя все компоненты реакции, но вместо клинического материала или препарата НК вносится соответствующее количество деионизованной воды или экстракта, не содержащего исследуемую ДНК \cite{Зорина:2012}. 
% Отрицательный контроль необходим для проверки компонентов реакции на отсутствие в них ДНК или клеток возбудителя вследствие контаминации и исключения учета ложноположительных результатов.
% % subparagraph отрицательный_контроль (end)

% \subparagraph{Внутренний контроль} % (fold)
% \label{par:внутренний_контроль}
% Препарат НК может содержать примеси ингибиторов, которые заметно снижают эффективность ПЦР, а в некоторых случаях могут приводить к полному отсутствию результатов исследования \cite{Зорина:2012}. 
% Кроме того, возможны ошибки на этапе составления РС (например, не добавили какой-либо компонент или саму НК), несоблюдение температурного режима хранения наборов реагентов или отдельных их частей (например, размораживание и потеря активности ферментов) и ряд других технических моментов, которые напрямую влияют на результаты ПЦР. 
% Поэтому становится необходимым контролировать ход амплификации в каждой пробирке с реакционной смесью. 
% Для этого в состав РС вводят дополнительный внутренний контроль (ВК).

% ВК -- это искусственно сконструированный препарат ДНК или РНК, который имеет принципиально отличную от искомой олигонуклеотидную последовательность.
% Для ВК в состав РС вводят собственные, строго комплементарные праймеры.

% Концентрация ВК в РС должна быть такой, чтобы не составлять конкуренции для амплификации даже единичных искомых молекул НК.

% Наличие ампликонов ВК является свидетельством нормального прохождения реакции амплификации. 
% Если ампликоны искомой НК отсутствуют, но не образовались также и ампликоны ВК, можно сделать вывод о технологических нарушениях либо о наличии в анализируемом образце нежелательных примесей. 
% В любом случае результат реакции следует признать недостоверным.

% ВК может быть использован не только непосредственно в составе РС для амплификации, но и для контроля качества выделения НК. 
% Для этого его вводят в каждую пробирку с исходным или предварительно обработанным образцом, проводят через этап выделения и только потом добавляют в РС. 
% Введение ВК с известной концентрацией на этапе выделения особенно важно для контроля количественного ПЦР-анализа.
% subparagraph внутренний_контроль (end)

% paragraph дополнительные_компоненты (end)

\paragraph{Основные этапы ПЦР} % (fold)
\label{par:основные_этапы_пцр}

Если в анализируемом образце присутствует искомая ДНК, то в процессе реакции амплификации с ней происходят изменения, которые обеспечиваются определенными температурными циклами. 
Каждый цикл амплификации состоит из трех этапов \cite{Зорина:2012}:
\begin{enumerate}
	\item \emph{Денатурация} -- это переход ДНК из двухнитевой формы в однонитевую при разрыве водородных связей между комплементарными парами оснований противоположных цепей ДНК под воздействием высоких температур.
	% \item \red{\emph{Отжиг} -- это присоединение праймеров к одноцепочечной ДНК-мишени. Праймеры подбирают так, чтобы они ограничивали искомый фрагмент ДНК и были комплементарны противоположным цепям ДНК. Отжиг происходит в соответствии с правилом комплементарности Чаргаффа. Если это условие не соблюдено, то отжига праймеров не происходит.}
	\item \emph{Отжиг} -- это добавление праймеров к одноцепочечной ДНК-мишени. Праймеры подобраны таким образом, чтобы они были способны ограничивать исходных фрагмент ДНК и были комплиментарными к противоположным цепям ДНК. Процесс отжига проходит согласно правилу комплиментарности Чаргаффа. В случае несоблюдения данного условия, успешного отжига не происходит.
	\item \emph{Элонгация (синтез)}. После отжига праймеров Taq-полимераза начинает достраивание второй цепи ДНК с $3'$-конца праймера с использованием дНТФ.
\end{enumerate}

Температурный цикл амплификации многократно повторяется (30 и более раз).
На каждом цикле количество синтезированных копий фрагмента ДНК удваивается (рис. \ref{fig:03_03_принцип_экспоненциальной_амплификации}).

Результатом циклического процесса является экспоненциальное увеличение количества специфического фрагмента ДНК, которое можно описать формулой \cite{Зорина:2012}:
\[
	A = M \cdot (2n - n - 1) \cdot 2n,
\]
где $A$ -- количество специфических (ограниченных праймерами) продуктов реакции амплификации; $M$ -- начальное количество ДНК-мишеней; $n$ -- число циклов амплификации.

Реальное значение эффективности отдельных циклов амплификации составляет, по некоторым данным, \SIrange{78}{97}{\percent}  \cite{Зорина:2012}. 
Если в пробе присутствуют ингибиторы реакции, это значение может быть намного меньше, поэтому фактическое количество специфических продуктов амплификации лучше описывает формула:
\[
	A = M \cdot (1 + E) \cdot n,
\]
где $E$ -- значение эффективности реакции.

Таким образом, специфические фрагменты, ограниченные на концах праймерами, впервые появляются в конце второго цикла, накапливаются в геометрической прогрессии и очень скоро начинают доминировать среди продуктов амплификации.

\subparagraph{Эффект плато} % (fold)
\label{spar:эффект_плато}

Процесс накопления специфических продуктов амплификации по геометрической прогрессии идет лишь ограниченное время, а затем его эффективность критически падает -- проявляется эффект плато.

Термин ``эффект плато'' используют для описания процесса накопления продуктов ПЦР на последних циклах амплификации, когда количество ампликонов достигает \SIrange{0.3}{1}{\pico\mole}.

На достижение эффекта плато влияют:
\begin{itemize}
	\item утилизация субстратов (дНТФ и праймеров);
	\item стабильность реагентов (дНТФ и фермента);
	\item количество ингибиторов, включая пирофосфаты и ДНК-дуплексы;
	\item неспецифические продукты и праймер-димеры, конкурирующие за праймеры, дНТФ и полимеразу;
	\item концентрация специфического продукта за счет неполной денатурации при высокой концентрации ампликонов.
\end{itemize}
% subparagraph эффект_плато (end)

% subsubsection основные_этапы_пцр (end)

% subsection полимеразная_цепная_реакция (end)

% subsection процессы_с_участием_нуклеиновых_кислот (end)