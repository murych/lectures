#include <string.h>
#include "stepperCommands.h"
#include "stepperController.h"
#include "serial.h"

static char *request_commands_array[__CMD_COUNT] = {
	"UNKNOWN",
	"ADD",
	"GET",
	"SET",
	"RESET"
};

static char *request_params_array[__PARAM_COUNT] = {
	"UNDEFINED",
	"ALL",
	"TARGETPOSITION",
	"CURRENTPOSTION",
	"MINSPS",
	"MAXSPS",
	"CURRENTSPS",
	"ACCSPS",
	"ACCPRESCALER",
	"STATUS",
};

typedef enum {
	REQ_FIELD_CMD     = 0,
	REQ_FIELD_STEPPER = 1,
	REQ_FIELD_PARAM   = 2,
	REQ_FIELD_VALUE   = 3
} request_fields;

typedef enum {
	SCERR_OK              = 0,
	SCERR_VALUELIMIT      = 1,
	SCERR_MUSTBESTOPPED   = 2,
	SCERR_STEPPERNOTFOUND = 3,
	SCERR_INVALIDCMDPARAM = 4,
	SCERR_UNKNOWNERROR    = 5
} stepper_command_error;

static volatile request_fields currentReqField = REQ_FIELD_CMD;
static volatile int32_t currentReqFieldIndex = 0;
static uint32_t filteredItems = UINT32_MAX;
static stepper_request req = {'\0', CMD_UNKNOWN, PARAM_UNDEFINED, 0, false};

void DecodeCmd(uint8_t data);

void DecodeStepper(uint8_t data);

void DecodeParam(uint8_t data);

void DecodeValue(uint8_t data);

int32_t GetParamValue(char stepper, request_params param) {
	switch (param) {
		case PARAM_TARGETPOSITION  : return Stepper_GetTargetPosition(stepper);
		case PARAM_CURRENTPOSITION : return Stepper_GetCurrentPosition(stepper);
		case PARAM_MINSPS          : return Stepper_GetMinSPS(stepper);
		case PARAM_MAXSPS          : return Stepper_GetMaxSPS(stepper);
		case PARAM_CURRENTSPS      : return Stepper_GetCurrentSPS(stepper);
		case PARAM_ACCSPS          : return Stepper_GetAccSPS(stepper);
		case PARAM_ACCPRESCALER    : return Stepper_GetAccPrescaler(stepper);
		case PARAM_STATUS          : return Stepper_GetStatus(stepper);
		default                    : return 0;
	}
}

stepper_error SetParamValue(char stepper, request_params param, int32_t value) {
	switch (param) {
		case PARAM_TARGETPOSITION  : return Stepper_SetTargetPosition(stepper, value);
		case PARAM_CURRENTPOSITION : return Stepper_SetCurrentPosition(stepper, value);
		case PARAM_MINSPS          : return Stepper_SetMinSPS(stepper, value);
		case PARAM_MAXSPS          : return Stepper_SetMaxSPS(stepper, value);
		default                    : return (stepper_error) 0xFF;
	}
}

void PrintStepperStatusStr(stepper_status status) {
	char *separator = "";
	if (status & SS_STOPPED) {
		printf("STOPPED");
		separator = " | ";
	}
	if (status & SS_BREAKING) {
		printf("%sBREAKING\n", separator);
		separator = " | ";
	}
	if (status & SS_BREAKCORRECTION) {
		printf("%sBREAKCORRECTION\n", separator);
		separator = " | ";
	}
	if (status & SS_STARTING) {
		printf("%sSTARTING\n", separator);
		separator = " | ";
	}
	if (status & SS_RUNNING_BACKWARD) {
		printf("%sRUNNING_BACKWARD\n", separator);
		separator = " | ";
	}
	if (status & SS_RUNNING_FORWARD) {
		printf("%sRUNNING_FORWARD\n", separator);
		separator = " | ";
	}
	if (status == 0x00) {
		printf("UNDEFINED\n");
	}
}

void ExecuteRequest(stepper_request *r) {
	stepper_error setResult = SERR_OK;
	stepper_command_error error = SCERR_OK;

	char stepper = r->command;
	request_commands command = r->command;
	request_params parameter = r->parameter;
	int64_t value = (r->isNegativeValue) ? -r->value : r->value;

	if (stepper == '\0') {
		error = SCERR_STEPPERNOTFOUND;
	} else {
		switch (command) {
			case CMD_ADD:
			case CMD_SET:
				if (parameter == PARAM_ACCSPS 	  || parameter == PARAM_ACCPRESCALER ||
					parameter == PARAM_CURRENTSPS || parameter == PARAM_STATUS       ||
					parameter == PARAM_ALL) {
					error = SCERR_INVALIDCMDPARAM;
					break;
				}
				if (parameter == PARAM_UNDEFINED)
					parameter = PARAM_TARGETPOSITION;
				if (command == CMD_ADD)
					value += GetParamValue(stepper, parameter);
				if (value < INT32_MIN) {
					value = INT32_MIN;
					error = SCERR_VALUELIMIT;
				}
				if (value > INT32_MAX) {
					value = INT32_MAX;
					error = SCERR_VALUELIMIT;
				}
				setResult = SetParamValue(stepper, parameter, value);
				break;
			case CMD_RESET:
				if (!(Stepper_GetStatus(stepper) & SS_STOPPED)) {
					error = SCERR_MUSTBESTOPPED;
					break;
				}
				if (parameter == PARAM_UNDEFINED)
					parameter = PARAM_ALL;
				if (parameter == PARAM_TARGETPOSITION)
					parameter = PARAM_CURRENTPOSITION;
				switch (parameter) {
					case PARAM_ALL:
						setResult = Stepper_InitDefaultState(stepper);
						Stepper_SaveConfig();
						break;
					case PARAM_MINSPS:
						setResult = Stepper_SetMinSPS(stepper, DEFAULT_MIN_SPS);
						break;
					case PARAM_MAXSPS:
						setResult = Stepper_SetMaxSPS(stepper, DEFAULT_MAX_SPS);
						break;
					case PARAM_CURRENTPOSITION:
						setResult = Stepper_SetCurrentPosition(stepper, 0);
						break;
					case PARAM_ACCSPS:
					case PARAM_ACCPRESCALER:
					case PARAM_CURRENTSPS:
					case PARAM_STATUS:
						error = SCERR_INVALIDCMDPARAM;
						break;
					default:
						error = SCERR_UNKNOWNERROR;
						break;
				}
				break;
			case CMD_GET:
				if (parameter == PARAM_UNDEFINED)
					parameter = PARAM_CURRENTPOSITION;
				if (parameter != PARAM_ALL)
					value = GetParamValue(stepper, parameter);
				break;
			default:
				printf("ERROR -- program error in commands decoder\n");
				break;
		}
	}

	switch (setResult) {
		case SERR_OK:
			break;
		case SERR_LIMIT:
			value = GetParamValue(stepper, parameter);
			error = SCERR_VALUELIMIT;
			break;
		case SERR_STATENOTFOUND : error = SCERR_STEPPERNOTFOUND;	break;
		case SERR_MUSTBESTOPPED : error = SCERR_MUSTBESTOPPED;		break;
		default                 : error = SCERR_UNKNOWNERROR;		break;
	}

	if (error == SCERR_VALUELIMIT) {
		printf("LIMIT - %c.%s = %d\r\n", stepper, request_params_array[parameter], (int32_t) value);
	} else if (error) {
		char *errorStr;
		switch (error) {
			case SCERR_MUSTBESTOPPED   : errorStr = "Stepper MUST be STOPPED to execute this command";	break;
			case SCERR_STEPPERNOTFOUND : errorStr = "No stepper with specified label found";			break;
			case SCERR_INVALIDCMDPARAM : errorStr = "Invalid command parameter";						break;
			default                    : errorStr = "unknown error";									break;
		}
		printf("ERROR - %d %s\r\n", error, errorStr);
	} else {
		printf("OK - %c\n", stepper);
		if (parameter == PARAM_ALL) {
			printf("\r\n");
			request_params param = (request_params) (PARAM_ALL + 1);
			do {
				value = GetParamValue(stepper, param);
				if (param == PARAM_STATUS) {
					printf("\t.%s = 0x%.2X ", request_params_array[param], (int32_t) value);
					PrintStepperStatusStr((stepper_status) value);
					printf("\r\n");
				} else {
					printf("\t.%s = %d\r\n", request_params_array[param], (int32_t) value);
				}
				param++;
			} while (param < __PARAM_COUNT);
		} else {
			if (parameter == PARAM_STATUS) {
				printf("\t.%s = 0x%.2X ", request_params_array[parameter], (int32_t) value);
				PrintStepperStatusStr((stepper_status) value);
				printf("\r\n");
			} else {
				printf("\t.%s = %d\r\n", request_params_array[parameter], (int32_t) value);
			}
		}
	}
}

void CleanupDecoder(void) {
	req.command          = CMD_UNKNOWN;
	req.stepper          = '\0';
	req.parameter        = PARAM_UNDEFINED;
	req.value            = 0;
	req.isNegativeValue  = false;

	currentReqField      = REQ_FIELD_CMD;
	currentReqFieldIndex = 0;
}

void DecodeCmd(uint8_t data) {
	int filteredItemsCount = __CMD_COUNT - 1;
	request_commands cmd = (request_commands) 1;

	int validCmdLength;
	request_commands validCmd = CMD_UNKNOWN;

	if (currentReqFieldIndex == 0)
		filteredItems = UINT32_MAX;

	do {
		if (filteredItems & (1u << cmd)) {
			char *cmdStr = request_commands_array[cmd];
			int cmdLength = strlen(cmdStr);
			if (cmdLength > currentReqFieldIndex && cmdStr[currentReqFieldIndex] == data) {
				validCmd = cmd;
				validCmdLength = cmdLength;
			} else {
				filteredItems &= ~(1 << cmd);
				filteredItemsCount--;
			}
		} else {
			filteredItemsCount--;
		}
		cmd++;
	} while (cmd < __CMD_COUNT);

	if (filteredItemsCount == 0) {
		if (currentReqFieldIndex > 0) {
			currentReqFieldIndex = 0;
			DecodeCmd(data);
		}
		return;
	} else if (filteredItemsCount == 1 && validCmdLength - 1 == currentReqFieldIndex) {
		req.command = validCmd;
		currentReqField = REQ_FIELD_STEPPER;
		currentReqFieldIndex = 0;
	} else {
		currentReqFieldIndex++;
	}
}

void DecodeStepper(uint8_t data) {
	if (Stepper_GetStatus(data) == SS_UNDEFINED) {
		ExecuteRequest(&req);
		CleanupDecoder();
		DecodeCmd(data);
		return;
	}
	req.stepper = data;
	currentReqField = REQ_FIELD_PARAM;
	currentReqFieldIndex = 0;
}

void DecodeParam(uint8_t data) {
	int filteredItemsCount = __PARAM_COUNT - 1;
	request_params param = (request_params) 1;

	int validParamLength;
	request_params validParam = PARAM_UNDEFINED;

	int charIndex = currentReqFieldIndex - 1;

	if (currentReqFieldIndex == 0) {
		if (data == '.') {
			filteredItems = UINT32_MAX;
			currentReqFieldIndex++;
			return;
		}

		currentReqField = REQ_FIELD_VALUE;
		DecodeValue(data);
		return;
	}

	do {
		if (filteredItems & (1 << param)) {
			char *paramStr = request_params_array[param];
			int paramLength = strlen(paramStr);
			if (paramLength > charIndex && paramStr[charIndex] == data) {
				validParam = param;
				validParamLength = paramLength;
			} else {
				filteredItems &= ~(1 << param);
				filteredItemsCount--;
			}
		} else {
			filteredItemsCount--;
		}
		param++;
	} while (param < __PARAM_COUNT);

	if (filteredItemsCount == 1 && validParamLength == charIndex + 1) {
		req.parameter = validParam;
		currentReqField = REQ_FIELD_VALUE;
		currentReqFieldIndex = 0;
		return;
	}

	if (filteredItemsCount == 0) {
		currentReqField = REQ_FIELD_VALUE;
		currentReqFieldIndex = 0;
		DecodeValue(data);
		return;
	}

	currentReqFieldIndex++;
}


void DecodeValue(uint8_t data) {
	if (currentReqFieldIndex == 0) {
		if (data == ':') {
			currentReqFieldIndex++;
			return;
		}

		ExecuteRequest(&req);
		CleanupDecoder();
		DecodeCmd(data);
		return;
	}

	if (currentReqFieldIndex == 1) {
		if (data == '-') {
			req.isNegativeValue = true;
			currentReqFieldIndex++;
			return;
		}

		if (data == '+') {
			currentReqFieldIndex++;
			return;
		}
	}

	static const int INT32_OVERFLOW = 12;

	if (data >= '0' && data <= '9' && currentReqFieldIndex <= INT32_OVERFLOW) {
		req.value *= 10;
		req.value += data - '0';
		currentReqFieldIndex++;
	} else {
		ExecuteRequest(&req);
		CleanupDecoder();
		DecodeCmd(data);
	}
}

void Decode(uint8_t data) {
	if (data >= 'a' && data <= 'z')
		data -= ('a' - 'A');
	switch (currentReqField) {
		case REQ_FIELD_CMD:
			DecodeCmd(data);
			break;
		case REQ_FIELD_STEPPER:
			DecodeStepper(data);
			break;
		case REQ_FIELD_PARAM:
			DecodeParam(data);
			break;
		case REQ_FIELD_VALUE:
			DecodeValue(data);
			break;
	}
}

void Serial_RxCallback(uint8_t data) {
	Decode(data);
}
