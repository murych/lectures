#include <string.h>
#include "stepperController.h"

static stepper_state steppers[MAX_STEPPERS_COUNT];
static int32_t initializedSteppersCount;

void SetAccelerationByMinSPS(stepper_state *stepper) {
	float fAccSPS = ACCSPS_TO_MINSPS_RATIO * STEP_CONTROLLER_PERIOD_US * stepper->minSPS / 100000.0f;

	if (fAccSPS > 10.0f) {
		stepper->stepCtrlPrescallerTicks = stepper->stepCtrlPrescaller = 1;
		stepper->accelerationSPS = fAccSPS;
	} else {
		uint32_t prescallerValue = 1;
		float prescaledAccSPS = fAccSPS;
		float remainder = prescaledAccSPS - (uint32_t)prescaledAccSPS;

		while (prescaledAccSPS < 0.9f || (0.1f < remainder & remainder < 0.9f)) {
			prescallerValue++;
			prescaledAccSPS += fAccSPS;
			remainder = prescaledAccSPS - (uint32_t)prescaledAccSPS;
		}

		stepper->stepCtrlPrescallerTicks = stepper->stepCtrlPrescaller = prescallerValue;
		stepper->accelerationSPS = prescaledAccSPS;

		if (remainder > 0.9f)
			stepper->accelerationSPS += 1;
	}
}

void SetStepTimerByCurrentSPS(stepper_state *stepper) {
	if (stepper->STEP_TIMER != NULL && stepper->STEP_TIMER->Instance != NULL) {
		TIM_TypeDef *timer = stepper->STEP_TIMER->Instance;
		uint32_t prescaler = 0;
		uint32_t timerTicks = STEP_TIMER_CLOCK / stepper->currentSPS;

		if(timerTicks > 0xFFFF) {
			prescaler = timerTicks / 0xFFFF;
			timerTicks /= (prescaler + 1);
		}
		timer->PSC = prescaler;
		timer->ARR = timerTicks;
	}
}

void DecrementSPS(stepper_state *stepper) {
	if (stepper->currentSPS > stepper->minSPS) {
		stepper ->currentSPS -= stepper->accelerationSPS;
		SetStepTimerByCurrentSPS(stepper);
	}
}

void IncrementSPS(stepper_state *stepper) {
	if(stepper->currentSPS < stepper->maxSPS) {
		stepper->currentSPS += stepper->accelerationSPS;
		SetStepTimerByCurrentSPS(stepper);
	}
}

stepper_state *GetState(char stepperName) {
	int32_t i = initializedSteppersCount;
	while (i--) {
		if (steppers[i].name == stepperName)
			return &steppers[i];
	}
	return (stepper_state *) NULL;
}

int32_t GetStepDirectionUnit(stepper_state *stepper) {
	return (stepper-> status & SS_RUNNING_BACKWARD) ? -1 : 1;
}

int64_t GetStepsToTarget(stepper_state *stepper) {
	return ((int64_t) stepper->targetPosition - (int64_t) stepper->currentPosition) * GetStepDirectionUnit(stepper);
}

stepper_error Stepper_SetupPeripherals(char stepperName, TIM_HandleTypeDef *stepTimer,
	uint32_t stepChannel, GPIO_TypeDef *dirGPIO, uint16_t dirPIN) {
	stepper_state *stepper = GetState(stepperName);
	if(stepper == NULL) {
		if (initializedSteppersCount == MAX_STEPPERS_COUNT) return SERR_NOMORESTATESAVALIABLE;
		stepper = &steppers[initializedSteppersCount++];
		stepper->name = stepperName;
		stepper->status = SS_STOPPED;
	} else if (!(stepper->status & SS_STOPPED)) {
		return SERR_MUSTBESTOPPED;
	}

	stepTimer->Instance->CR1 |= TIM_CR1_ARPE;

	stepper->STEP_TIMER = stepTimer;
	stepper->STEP_CHANNEL = stepChannel;
	stepper->DIR_GPIO = dirGPIO;
	stepper->DIR_PIN = dirPIN;

	return SERR_OK;
}

stepper_error Stepper_InitDefaultState(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL) {
		if (initializedSteppersCount == MAX_STEPPERS_COUNT) return SERR_NOMORESTATESAVALIABLE;
		stepper = &steppers[initializedSteppersCount++];
		stepper->name = stepperName;
		stepper->status = SS_STOPPED;
	} else if (!(stepper->status & SS_STOPPED)) {
		return SERR_MUSTBESTOPPED;
	}

	stepper->minSPS = DEFAULT_MIN_SPS;
	stepper->maxSPS = DEFAULT_MAX_SPS;
	stepper->currentSPS = stepper->minSPS;

	stepper->targetPosition = 0;
	stepper->currentPosition = 0;
	stepper->breakInitiationSPS = stepper->maxSPS;

	SetAccelerationByMinSPS(stepper);
	SetStepTimerByCurrentSPS(stepper);

	return SERR_OK;
}

void ExecuteController(stepper_state *stepper) {
	stepper_status status = stepper->status;

	if (status & SS_STOPPED) {
		if (stepper->targetPosition != stepper->currentPosition) {
			stepper->stepCtrlPrescallerTicks = stepper->stepCtrlPrescaller;
			stepper->status = SS_STARTING;
			stepper->STEP_TIMER->Instance->EGR = TIM_EGR_UG;
			HAL_TIM_PWM_Start(stepper->STEP_TIMER, stepper->STEP_CHANNEL);
		}
		return;
	}

	if (status == SS_STARTING)
		return;

	stepper->stepCtrlPrescallerTicks--;

	if(!(status & SS_BREAKING)) {
		float estimatedTimeToTarget = 2.0f * GetStepsToTarget(stepper) / (stepper->currentSPS + stepper->minSPS);
		int32_t spsSwitches = (stepper->currentSPS - stepper->minSPS) / stepper->accelerationSPS;
		float timeToReduceSpeed = 
			(((float)STEP_CONTROLLER_PERIOD_US) / 1000000.0f) *
			((int64_t)(stepper->stepCtrlPrescaller) * spsSwitches + stepper->stepCtrlPrescallerTicks);

		if (estimatedTimeToTarget <= timeToReduceSpeed) {
			stepper->breakInitiationSPS = stepper->currentSPS;
			stepper->status &= ~SS_BREAKCORRECTION;
			stepper->status |= SS_BREAKING;

			DecrementSPS(stepper);

			if (stepper->stepCtrlPrescallerTicks == 0)
				stepper->stepCtrlPrescallerTicks = stepper->stepCtrlPrescaller;

			return;
		}
	}

	if (stepper->stepCtrlPrescallerTicks == 0){
		if (status & SS_BREAKING) {
			int32_t spaSwitchesOnBreakInitiated = 
				(stepper->breakInitiationSPS - stepper->minSPS) / stepper->accelerationSPS;
			int32_t spsSwitchesLeft =
				(stepper->currentSPS - stepper->minSPS) / stepper->accelerationSPS;
			if (spaSwitchesOnBreakInitiated / 2 > spsSwitchesLeft && spsSwitchesLeft > 10) {
				stepper->status |= SS_BREAKCORRECTION;
				stepper->status &= ~SS_BREAKING;
			}
			DecrementSPS(stepper);
		} else if (!(status & SS_BREAKCORRECTION)) {
			IncrementSPS(stepper);
		}
		stepper->stepCtrlPrescallerTicks = stepper->stepCtrlPrescaller;
	}
}

void Stepper_PulseTimerUpdate(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL)
		return;

	switch (stepper->status & ~(SS_BREAKING | SS_BREAKCORRECTION)) {
		case SS_STARTING:
			if (stepper->currentPosition > stepper->targetPosition) {
				stepper->status = SS_RUNNING_BACKWARD;
				stepper->DIR_GPIO->BSRR = (uint32_t) stepper->DIR_PIN << 16u;
			} else if (stepper->currentPosition < stepper->targetPosition) {
				stepper->status = SS_RUNNING_FORWARD;
				stepper->DIR_GPIO->BSRR = stepper->DIR_PIN;
			} else if (stepper->currentPosition == stepper->targetPosition) {
				stepper->status = SS_STOPPED;
				HAL_TIM_PWM_Stop(stepper->STEP_TIMER, stepper->STEP_CHANNEL);
			}
		case SS_RUNNING_FORWARD:
		case SS_RUNNING_BACKWARD:
			stepper->currentPosition += GetStepDirectionUnit(stepper);
			{
				if (GetStepsToTarget(stepper) <= 0 && stepper->currentSPS == stepper->minSPS) {
					stepper->status = SS_STOPPED;
					HAL_TIM_PWM_Stop(stepper->STEP_TIMER, stepper->STEP_CHANNEL);
					printf("%c.stop:%d\r\n", stepper->name, stepper->currentPosition);
				}
			}
			break;
	}
}

void Stepper_ExecuteAllControllers(void) {
	int32_t i = initializedSteppersCount;
	if (i == 0)
		return;
	while (i--)
		ExecuteController(&steppers[i]);
}

stepper_error Stepper_SetTargetPosition(char stepperName, int32_t value) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL)
		return SERR_STATENOTFOUND;
	stepper->targetPosition =  value;
	return SERR_OK;
}

stepper_error Stepper_SetCurrentPosition(char stepperName, int32_t value) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL)
		return SERR_STATENOTFOUND;
	if (stepper->status & SS_STOPPED) {
		stepper->targetPosition = stepper->currentPosition = value;
		return SERR_OK;
	}
	return SERR_MUSTBESTOPPED;
}

stepper_error Stepper_SetMinSPS(char stepperName, int32_t value) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL)
		return SERR_STATENOTFOUND;
	if (stepper->status & SS_STOPPED) {
		stepper_error result = SERR_OK;
		if (value > DEFAULT_MAX_SPS) {
			stepper->minSPS = stepper->currentSPS = DEFAULT_MAX_SPS;
			result = SERR_LIMIT;
		} else if (value < DEFAULT_MIN_SPS) {
			stepper->minSPS = stepper->currentSPS = DEFAULT_MIN_SPS;
		} else {
			stepper->minSPS = stepper->currentSPS = value;
		}
		if (stepper->minSPS > stepper->maxSPS)
			stepper->maxSPS = stepper->minSPS;
		SetAccelerationByMinSPS(stepper);
		SetStepTimerByCurrentSPS(stepper);
		Stepper_SaveConfig();
		return result;
	}
	return SERR_MUSTBESTOPPED;
}

stepper_error Stepper_SetMaxSPS(char stepperName, int32_t value) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL)
		return SERR_STATENOTFOUND;
	if (stepper->status & SS_STOPPED) {
		stepper_error result = SERR_OK;
		if (value > DEFAULT_MAX_SPS) {
			stepper->maxSPS = DEFAULT_MAX_SPS;
			result = SERR_LIMIT;
		} else if (value < DEFAULT_MIN_SPS) {
			stepper->maxSPS = DEFAULT_MIN_SPS;
			result = SERR_LIMIT;
		} else {
			stepper->maxSPS = value;
		}
		if (stepper->minSPS > stepper->maxSPS)
			stepper->minSPS = stepper->currentSPS = stepper->maxSPS;
		SetAccelerationByMinSPS(stepper);
		SetStepTimerByCurrentSPS(stepper);
		Stepper_SaveConfig();
		return result;
	}
	return SERR_MUSTBESTOPPED;
}

stepper_error Stepper_SetAccSPS(char stepperName, int32_t  value) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL)
		return SERR_STATENOTFOUND;
	if (stepper->status & SS_STOPPED) {
		stepper_error result = SERR_OK;
		if (value > DEFAULT_MAX_SPS) {
			stepper->accelerationSPS = DEFAULT_MAX_SPS;
			result = SERR_LIMIT;
		} else if (value < DEFAULT_MIN_SPS) {
			stepper->accelerationSPS = DEFAULT_MIN_SPS;
			result = SERR_LIMIT;
		} else {
			stepper->accelerationSPS = value;
		}
		Stepper_SaveConfig();
		return result;
	}
	return SERR_MUSTBESTOPPED;
}

stepper_error Stepper_SetAccPrescaller(char stepperName, int32_t value) {
	stepper_state *stepper = GetState(stepperName);
	if (stepper == NULL)
		return SERR_STATENOTFOUND;
	if (stepper->status & SS_STOPPED) {
		stepper->stepCtrlPrescaller = (value < 1) ? 1 : value;
		Stepper_SaveConfig();
		return (value < 1) ? SERR_LIMIT : SERR_OK;
	}
	return SERR_MUSTBESTOPPED;
}

int32_t Stepper_GetTargetPosition(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? 0 : stepper->targetPosition;
}

int32_t Stepper_GetCurrentPosition(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? 0 : stepper->currentPosition;
}

int32_t Stepper_GetMinSPS(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? 0 : stepper->minSPS;
}

int32_t Stepper_GetMaxSPS(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? 0 : stepper->maxSPS;
}

int32_t Stepper_GetCurrentSPS(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? 0 : stepper->currentSPS;
}

int32_t Stepper_GetAccSPS(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? 0 : stepper->accelerationSPS;
}

int32_t Stepper_GetAccPrescaler(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? 0 : stepper->stepCtrlPrescaller;
}

stepper_status Stepper_GetStatus(char stepperName) {
	stepper_state *stepper = GetState(stepperName);
	return (stepper == NULL) ? SS_UNDEFINED : stepper->status;
}

void Stepper_SaveConfig(void) {
    uint32_t configAddr = FLASH_START_PAGE;
	int32_t i = 0;

	HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(
            FLASH_FLAG_EOP | FLASH_FLAG_WRPERR | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR
		);
	FLASH_Erase_Sector(FLASH_SECTOR_3, VOLTAGE_RANGE_3);
	for (i = 0; i < initializedSteppersCount; i++) {
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, configAddr, steppers[i].minSPS);
		configAddr += 4;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, configAddr, steppers[i].maxSPS);
		configAddr += 4;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, configAddr, steppers[i].accelerationSPS);
		configAddr += 4;
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, configAddr, steppers[i].stepCtrlPrescaller);
		configAddr +=4;
	}
	HAL_FLASH_Lock();
}

void Stepper_LoadConfig(void) {
	int32_t *configPtr = (int32_t *) ADDR_FLASH_SECTOR;
	int32_t i = 0;
	for (i = 0; i < initializedSteppersCount; i++) {
		steppers[i].currentSPS =
		steppers[i].minSPS = *configPtr++;
		steppers[i].maxSPS = *configPtr++;
		steppers[i].accelerationSPS = *configPtr++;
		steppers[i].stepCtrlPrescaller =
		steppers[i].stepCtrlPrescallerTicks = *configPtr++;
		SetStepTimerByCurrentSPS(&steppers[i]);
	}
}