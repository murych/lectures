#ifndef __MAIN_H__
#define __MAIN_H__

#define Y_MS_1_Pin GPIO_PIN_1
#define Y_MS_1_GPIO_Port GPIOC
#define Y_MS_2_Pin GPIO_PIN_2
#define Y_MS_2_GPIO_Port GPIOC
#define Y_MS_3_Pin GPIO_PIN_3
#define Y_MS_3_GPIO_Port GPIOC
#define UART_TX_Pin GPIO_PIN_2
#define UART_TX_GPIO_Port GPIOA
#define UART_RX_Pin GPIO_PIN_3
#define UART_RX_GPIO_Port GPIOA
#define Y_STOP_Pin GPIO_PIN_4
#define Y_STOP_GPIO_Port GPIOA
#define Z_STOP_Pin GPIO_PIN_5
#define Z_STOP_GPIO_Port GPIOA
#define PARK_MAGNET_Pin GPIO_PIN_6
#define PARK_MAGNET_GPIO_Port GPIOA
#define PARK_HEATER_Pin GPIO_PIN_7
#define PARK_HEATER_GPIO_Port GPIOA
#define Z_MS_1_Pin GPIO_PIN_0
#define Z_MS_1_GPIO_Port GPIOB
#define Z_MS_2_Pin GPIO_PIN_1
#define Z_MS_2_GPIO_Port GPIOB
#define Z_MS_3_Pin GPIO_PIN_2
#define Z_MS_3_GPIO_Port GPIOB
#define RESET_Pin GPIO_PIN_14
#define RESET_GPIO_Port GPIOB
#define SLEEP_Pin GPIO_PIN_15
#define SLEEP_GPIO_Port GPIOB
#define MOTORS_ENABLE_Pin GPIO_PIN_9
#define MOTORS_ENABLE_GPIO_Port GPIOA
#define Z_DIR_Pin GPIO_PIN_11
#define Z_DIR_GPIO_Port GPIOC
#define Z_STEP_Pin GPIO_PIN_12
#define Z_STEP_GPIO_Port GPIOC
#define Y_DIR_Pin GPIO_PIN_6
#define Y_DIR_GPIO_Port GPIOB
#define Y_STEP_Pin GPIO_PIN_7
#define Y_STEP_GPIO_Port GPIOB

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif