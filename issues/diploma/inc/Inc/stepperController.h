#include "stm32f1xx_hal.h"

#define ADDR_FLASH_SECTOR	((uint32_t)0x0800C000)
#define FLASH_START_PAGE    ((uint32_t)0x08008000)
#define FLASH_STOP_PAGE     ((uint32_t)0x0800C000)
#define FLASH_PAGE_SIZE     ((uint16_t)0x400)


#define MAX_STEPPERS_COUNT		10
#define ACCSPS_TO_MINSPS_RATIO	0.8f
#define DEFAULT_MIN_SPS			1
#define DEFAULT_MAX_SPS			400000

typedef enum {
	SS_UNDEFINED        = 0x00,
	SS_RUNNING_BACKWARD = 0x01,
	SS_RUNNING_FORWARD  = 0x02,
	SS_STARTING         = 0x04,
	SS_BREAKING         = 0x10,
	SS_BREAKCORRECTION  = 0x20,
	SS_STOPPED          = 0x80
} stepper_status;

typedef enum {
	SERR_OK                    = 0,
	SERR_NOMORESTATESAVALIABLE = 1,
	SERR_MUSTBESTOPPED         = 2,
	SERR_STATENOTFOUND         = 3,
	SERR_LIMIT                 = 4
} stepper_error;

typedef struct {
	char name;
	TIM_HandleTypeDef *STEP_TIMER;
	uint32_t STEP_CHANNEL;
	GPIO_TypeDef *DIR_GPIO;
	uint16_t DIR_PIN;
	volatile int32_t stepCtrlPrescaller;
	volatile int32_t stepCtrlPrescallerTicks;
	volatile int32_t minSPS;
	volatile int32_t maxSPS;
	volatile int32_t accelerationSPS;
	volatile int32_t breakInitiationSPS;
	volatile int32_t currentSPS;
	volatile int32_t targetPosition;
	volatile int32_t currentPosition;
	volatile stepper_status status;
} stepper_state;

extern uint32_t STEP_TIMER_CLOCK;
extern uint32_t STEP_CONTROLLER_PERIOD_US;

stepper_error Stepper_SetupPeripherals(char stepperName, TIM_HandleTypeDef *stepTimer,
	uint32_t stepChannel, GPIO_TypeDef *dirGPIO, uint16_t dirPIN);

stepper_error Stepper_InitDefaultState(char stepperName);
void Stepper_ExecuteAllControllers(void);
void Stepper_PulseTimerUpdate(char stepperName);

stepper_error Stepper_SetTargetPosition(char stepperName, int32_t value);
stepper_error Stepper_SetCurrentPosition(char stepperName, int32_t value);
stepper_error Stepper_SetMinSPS(char stepperName, int32_t value);
stepper_error Stepper_SetMaxSPS(char stepperName, int32_t value);
stepper_error Stepper_SetAccSPS(char stepperName, int32_t value);
stepper_error Stepper_SetAccPrescaller(char stepperName, int32_t value);

int32_t Stepper_GetTargetPosition(char stepperName);
int32_t Stepper_GetCurrentPosition(char stepperName);
int32_t Stepper_GetMinSPS(char stepperName);
int32_t Stepper_GetMaxSPS(char stepperName);
int32_t Stepper_GetCurrentSPS(char stepperName);
int32_t Stepper_GetAccSPS(char stepperName);
int32_t Stepper_GetAccPrescaler(char stepperName);
stepper_status Stepper_GetStatus(char stepperName);

void Stepper_LoadConfig(void);
void Stepper_SaveConfig(void);