#include <stdint.h>

typedef enum {
	SERIAL_TX = 0x01,
	SERIAL_TXOVERFLOW = 0x02,
	SERIAL_TXOVERFLOWMSG = 0x04,
	SERIAL_RX = 0x10
} serial_status;

struct __FILE {
	int dummy;
};

void Serial_WriteBytes(uint8_t *data, uint32_t length);
void Serial_WriteString(char *str);
void Serial_WriteInt(int32_t i);
void Serial_ExecutePendingTransmits(void);

void Serial_InitRxSequence(void);
void Serial_CheckRxTimeout(void);
void Serial_RxCallback(uint8_t byte);