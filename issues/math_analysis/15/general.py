import matplotlib.pyplot as plt
from matplotlib import rcParams
from numpy import arange, pi, sin, cos

rcParams['figure.figsize'] = (11.69, 8.27)
rcParams['figure.subplot.hspace'] = 0.5
rcParams['xtick.labelsize'] = 5
rcParams['ytick.labelsize'] = 5

fig = plt.figure()


def plot_setup():
	'''
	this function is setting up axes & putting ticks on them
	'''
	plt.axis('tight')

	X_limits = (-3.2*pi, 3.2*pi)
	X_ticks_values = [-3*pi, -2*pi, -2*pi+1, -pi, 0, 1, pi, 2*pi, 2*pi+1, 3*pi]
	X_ticks_labels = [r'$-3\pi$', r'$-2\pi$', r'$-2\pi+1$', r'$-\pi$', r'$0$',
		r'$1$', r'$\pi$', r'$2\pi$', r'$2\pi+1$', r'$3\pi$']

	y_limits = (-1.2, 1.2)
	y_ticks_values = [-1, 1]
	y_ticks_labels = [r'$-1$', r'$1$']

	ax = plt.gca()
	ax.spines['right'].set_color('none')
	ax.spines['top'].set_color('none')
	ax.xaxis.set_ticks_position('bottom')
	ax.spines['bottom'].set_position(('data', 0))
	ax.yaxis.set_ticks_position('left')
	ax.spines['left'].set_position(('data', 0))
	
	plt.xlim(X_limits)
	plt.ylim(y_limits)
	plt.xticks(X_ticks_values, X_ticks_labels)
	plt.yticks(y_ticks_values, y_ticks_labels)
	ax.text(2, 6, r'an equation: $E=mc^2$', fontsize=15)
	


def plot_support():
	'''
	this function plots support lines & points of function's break
	'''
	params = {'color': 'k', 'lw': 0.5, 'ls': '--'}
	X_support = [(-2*pi+1, -2*pi+1), (-pi,-pi), (1, 1), (pi,pi), (2*pi+1, 2*pi+1), (3*pi, 3*pi)]
	y_support = [(-1,1), (-1,0), (1,-1), (-1, 0), (1, -1), (-1, 0)]
	for X, y in zip(X_support, y_support):
		plt.plot(X, y, **params)

	X_dots = [-3*pi, -2*pi, -2*pi+1, -2*pi+1, -pi, -pi, 0, 1, 1, pi, pi, 2*pi, 2*pi+1, 2*pi+1, 3*pi]
	y_dots = [0, 0, 1, -1, -1, 0, 0, 1, -1, -1, 0, 0, 1, -1, -1]
	plt.plot(X_dots, y_dots, 'wo', zorder=3)


def plot_function():
	params = {'color': 'b', 'linewidth': 3, 'zorder': 3}

	function = (arange(0, 1, 0.001))**2

	X_function = [arange(-3*pi, -2*pi), arange(-2*pi, -2*pi+1, 0.001),
		arange(-2*pi+1, -pi), arange(-pi,0), arange(0,1,0.001), arange(1,pi), 
		arange(pi, 2*pi), arange(2*pi, 2*pi+1, 0.001), arange(2*pi+1, 3*pi)]

	y_function = [[0]*len(arange(-pi, 0)), function, [-1]*len(arange(1, pi))] * 3

	for X, y in zip(X_function, y_function):
		plt.plot(X, y, **params)


def plot_fourier(N, color):
	X = arange(-3*pi, 3*pi, 0.001)
	fourier = []

	a_0 = 4 / (3 * pi) - 1
	a_n = lambda n: (1/pi) * ( (2*sin(n))/n + (2*cos(n))/n**2 - (2*sin(n)/n**3) ) * cos(n*x)
	b_n = lambda n: (1/pi) * ( (2*cos(n)-2)/n**3 + (2*sin(n))/n**2 - (2*cos(n)-(-1)**n)/n ) * sin(n*x)

	for x in X:
		fourier.append(a_0/2 + sum(map(a_n, N)) + sum(map(b_n, N)))

	plt.plot(X, fourier, color, zorder=4)

for plot, n, color in zip([1, 2, 3], [2, 10, 100], ['y', 'g', 'r']):
	plt.subplot(3, 1, plot)
	plt.title(r'$S(x) = \frac{\frac{4}{3\pi}-1}{2} + \sum^{%d}_{n=1}\left[\frac{1}{\pi}\left(\frac{2(\cos n -1)}{n^3} + \frac{2\sin n}{n^2} - \frac{2\cos n - (-1)^n}{n}\right)\sin(nx) + \frac{1}{\pi} \left(\frac{2\sin n}{n} + \frac{2\cos n}{n^2} - \frac{2\sin n}{n^3}\right)\cos(nx)\right]$' % (n))
	plot_setup()
	plot_function()
	plot_support()
	# plot_fourier(range(1, n), color)


# plt.show()

fig.savefig('general_w_row.png', dpi=800)
