'''
разложение функции в ряд по косинусам
(как нечетную функцию на полупериоде)
используются только коэффициенты a_0 и a_n,
с множителем 2/pi
'''

import matplotlib.pyplot as plt
from matplotlib import rcParams
from numpy import arange, pi, sin, cos

rcParams['figure.subplot.hspace'] = 0.5
rcParams['xtick.labelsize'] = 5
rcParams['ytick.labelsize'] = 5

fig = plt.figure()
 

def plot_setup():
	'''
	this function is setting up axes & putting ticks on them
	'''
	plt.axis('equal')

	x_limits = (-3*pi, 3*pi)
	x_ticks_values = [-2*pi-1, -2*pi, -2*pi+1, -pi, -1, 0, 1, pi, 2*pi-1, 2*pi, 2*pi+1]
	x_ticks_labels = [r'$-2\pi-1$', r'$-2\pi$', r'$-2\pi+1$', r'$-\pi$', r'$-1$',
		r'$0$', r'$1$', r'$\pi$', r'$2\pi-1$', r'$2\pi$', r'$2\pi+1$']

	y_limits = (-1.5, 1.5)
	y_ticks_values = [-1, 1]
	y_ticks_labels = [r'$-1$', r'$1$']

	ax = plt.gca()
	ax.spines['right'].set_color('none')
	ax.spines['top'].set_color('none')
	ax.xaxis.set_ticks_position('bottom')
	ax.spines['bottom'].set_position(('data', 0))
	ax.yaxis.set_ticks_position('left')
	ax.spines['left'].set_position(('data', 0))
	
	plt.xlim(x_limits)
	plt.ylim(y_limits)
	plt.xticks(x_ticks_values, x_ticks_labels)
	plt.yticks(y_ticks_values, y_ticks_labels)


def plot_support():
	'''
	this function plots support lines & points of function's break
	'''
	params = {'color': 'k', 'lw':0.5, 'ls': '--'}
	x_support = [(-2*pi+1, -2*pi+1), (-1, -1), (1, 1), (2*pi-1, 2*pi-1)]
	y_support = [(1,-1), (1,-1), (1,-1), (1, -1)]
	for X, y in zip(x_support, y_support):
		plt.plot(X, y, **params)

	x_dots = [-2*pi-1, -2*pi, -2*pi+1, -2*pi+1, -pi, -1, -1, 0, 1, 1, pi, 2*pi-1, 2*pi-1, 2*pi, 2*pi+1]
	y_dots = [1, 0, 1, -1, -1, -1, 1, 0, 1, -1, -1, -1, 1, 0, 1]
	plt.plot(x_dots, y_dots, 'wo')


def plot_function():
	params = {'color':'b', 'linewidth': 2}

	function = list(arange(0, 1, 0.001)**2)
	function_ = list(arange(-1, 0, 0.001)**2)

	X_function = [arange(-2*pi-1, -2*pi, 0.001), arange(-2*pi, -2*pi+1, 0.001),
		arange(-2*pi+1, -1, 0.1), arange(-1,0,0.001), arange(0,1,0.001), 
		arange(1, 2*pi-1, 0.1), arange(2*pi-1, 2*pi, 0.001), arange(2*pi, 2*pi+1, 0.001)]

	y_function = [function_, function, [-1]*len(arange(-2*pi+1, -1, 0.1)), function_, function, [-1]*len(arange(1, 2*pi-1, 0.1)), function_, function]

	for X, y in zip(X_function, y_function):
		plt.plot(X, y, **params)


def plot_fourier(N, color):
	X = arange(-2*pi-2, 2*pi+1, 0.001)
	fourier = []
	a_0 = 4/(3*pi) - 1
	a_n = lambda n: (2/pi) * ( (2*sin(n))/n + (2*cos(n))/n**2 - (2*sin(n))/n**3 ) * cos(n*x)

	for x in X:
		fourier.append(a_0 + sum(list(map(a_n, N))))
	plt.plot(X, fourier, color)


for plot, n, color in zip([1, 2, 3], [2, 10, 100], ['y', 'g', 'r']):
	plt.subplot(3, 1, plot)
	plt.title(r'$S(x) = \frac{\frac{4}{3\pi}-1}{2} + \sum^{%d}_{n=1} \left[ \frac{2}{\pi} \left(\frac{2\sin n}{n} + \frac{2\cos n}{n^2} - \frac{2\sin n}{n^3}\right)\cos(nx) \right]$' % (n))
	plot_setup()
	plot_function()
	plot_support()
	plot_fourier(range(1, n), color)

plt.show()

# fig.savefig('general_cos_all_rows.png', dpi=800)
