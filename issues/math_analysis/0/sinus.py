# -*- coding: UTF-8 -*-
'''
разложение функции в ряд по синусам
(как нечетную функцию на полупериоде)
используются только коэффициенты b_n,
с множителем 2/pi
'''
import matplotlib.pyplot as plt
from matplotlib import rcParams
from numpy import arange, pi, sin, cos

rcParams['figure.subplot.hspace'] = 0.5
rcParams['figure.figsize'] = (11.69, 8.27)
rcParams['font.family'] = 'fantasy'
rcParams['font.fantasy'] = 'Arial'

fig = plt.figure()


def plot_setup():
	'''
	this function is setting up axes & putting ticks on them
	'''
	plt.axis('tight')
	# plt.axis('equal')

	X_limits = (-9.5, 9.5)
	X_ticks_values = range(-9, 10)
	X_ticks_labels = [r'$%d$' % x for x in range(-9, 10)]

	y_limits = (-1.5, 1.5)
	y_ticks_values = range(-1,2,2)
	y_ticks_labels = [r'$%d$' % x for x in range(-1,2,2)]

	ax = plt.gca()
	ax.grid()

	ax.spines['right'].set_color('none')
	ax.spines['top'].set_color('none')
	ax.xaxis.set_ticks_position('bottom')
	ax.spines['bottom'].set_position(('data', 0))
	ax.yaxis.set_ticks_position('left')
	ax.spines['left'].set_position(('data', 0))
	
	plt.xlim(X_limits)
	plt.ylim(y_limits)
	plt.xticks(X_ticks_values, X_ticks_labels)
	plt.yticks(y_ticks_values, y_ticks_labels)


def plot_function():
	params = {'color': 'b', 'linewidth': 3, 'zorder': 3}

	X_function = [arange(-9,-7,0.1), arange(-7,-6,0.1), arange(-6,-5,0.1), arange(-5,-3,0.1),
		arange(-3,-1,0.1), arange(-1,0,0.1), arange(0,1,0.1), arange(1,3,0.1),
		arange(3, 5, 0.1), arange(5, 6,0.1), arange(6,7,0.1), arange(7,9,0.1)]

	y_function = [[0]*len(arange(-3,-1,0.1)), [-1]*len(arange(-1,0,0.1)), [1]*len(arange(0,1,0.1)), [0]*len(arange(1,3,0.1))] * 3

	for X, y in zip(X_function, y_function):
		plt.plot(X, y, **params)


	X_dots = [-9, -7, -7, -6,- 6, -5, -5, -3, -1, -1, 0, 0, 1, 1, 3, 5, 5, 6, 6, 7, 7, 9]
	y_dots = [0, 0, -1, -1, 1, 1, 0, 0, 0, -1, -1, 1, 1, 0, 0, 0, -1, -1, 1, 1, 0, 0]

	plt.plot(X_dots, y_dots, 'wo', zorder=3)


def plot_fourier(N, color):
	X = arange(-9, 9, 0.001)
	y = []

	b_n = lambda n: 2/(pi*n) * ( 1 - cos(pi*n/3) ) * sin(pi*n*x/3)

	for x in X:
		y.append(sum(map(b_n, N)))

	# plt.plot(X, y, color, zorder=4)
	label = u'Сумма %d гармоник'%(len(N))
	plt.plot(X, y, color, zorder=4, label=label, lw=0.7)

plots = [1, 2, 3]
N = [2, 10, 100]
colors = ['y', 'g', 'r']

# plt.suptitle(u'Разложение в ряд по синусам')

# for plot, n, color in zip(plots, N, colors):
# 	plt.subplot(3, 1, plot)
# 	plt.title(u'Сумма %d гармоник'%(n))
# 	plot_setup()
# 	plot_function()
# 	plot_fourier(range(1, n), color)

# plt.show()

plot_setup()
plot_function()
for n, color, in zip(N, colors):
	plot_fourier(range(1, n), color)

plt.legend(loc='lower right', prop={'size':10})

fig.savefig('sinus_merge.png', dpi=800)
