# -*- coding: UTF-8 -*-
'''
разложение функции в ряд по синусам
(как нечетную функцию на полупериоде)
используются только коэффициенты b_n,
с множителем 2/pi
'''
import matplotlib.pyplot as plt
from matplotlib import rcParams
from numpy import arange, pi, sin, cos

rcParams['figure.figsize'] = (11.69, 8.27)
rcParams['font.family'] = 'fantasy'
rcParams['font.fantasy'] = 'Arial'

fig = plt.figure()


def plot_setup():
	'''
	this function is setting up axes & putting ticks on them
	'''
	plt.axis('tight')
	# plt.axis('equal')

	X_limits = (-6.5, 6.5)
	X_ticks_values = range(-6, 7)
	X_ticks_labels = [r'$%d$' % x for x in range(-6, 7)]

	y_limits = (-1.5, 1.5)
	y_ticks_values = range(-1,2,2)
	y_ticks_labels = [r'$%d$' % x for x in range(-1,2,2)]

	ax = plt.gca()
	ax.grid()

	ax.spines['right'].set_color('none')
	ax.spines['top'].set_color('none')
	ax.xaxis.set_ticks_position('bottom')
	ax.spines['bottom'].set_position(('data', 0))
	ax.yaxis.set_ticks_position('left')
	ax.spines['left'].set_position(('data', 0))
	
	plt.xlim(X_limits)
	plt.ylim(y_limits)
	plt.xticks(X_ticks_values, X_ticks_labels)
	plt.yticks(y_ticks_values, y_ticks_labels)


def plot_function():
    params = {'color': 'b', 'linewidth': 3, 'zorder': 3}

    X_function = [
        arange(-6,-4.6,0.1), arange(-4.6,-4,0.1), arange(-4,-3.4,0.1), arange(-3.4,-2,0.1),
        arange(-2,-0.6,0.1), arange(-0.6,0,0.1), arange(0,0.6,0.1), arange(0.6,2,0.1),
        arange(2, 3.6, 0.1), arange(3.6, 4,0.1), arange(4,4.6,0.1), arange(4.6,6,0.1)
        ]

    y_function = [
        [0]*len(arange(-6,-4.6,0.1)), [-1]*len(arange(-4.6,-4,0.1)), [1]*len(arange(-4,-3.4,0.1)), [0]*len(arange(-3.4,-2,0.1)),
        [0]*len(arange(-2,-0.6,0.1)), [-1]*len(arange(-0.6,0,0.1)), [1]*len(arange(0,0.6,0.1)), [0]*len(arange(0.6,2,0.1)),
        [0]*len(arange(2, 3.6, 0.1)), [-1]*len(arange(3.6, 4,0.1)), [1]*len(arange(4,4.6,0.1)), [0]*len(arange(4.6,6,0.1))
        ]

    for X, y in zip(X_function, y_function):
        plt.plot(X, y, **params)

    X_dots = [-6, -4.6, -4.6, -4, -4, -3.4, -3.4, -2, -0.6, -0.6, 0, 0, 0.6, 0.6, 2, 3.4, 3.4, 4, 4, 4.6, 4.6, 6]
    y_dots = [0, 0, -1, -1, 1, 1, 0, 0, 0, -1, -1, 1, 1, 0, 0, 0, -1, -1, 1, 1, 0, 0]

    plt.plot(X_dots, y_dots, 'wo', zorder=3)


def plot_fourier(N, color):
	X = arange(-6, 6, 0.001)
	y = []

	b_n = lambda n: (-2/(pi*n)) * (cos(0.3*pi*n) - 1) * sin(pi*n*x/2)

	for x in X:
		y.append(sum(map(b_n, N)))

	label = u'Сумма %d гармоник'%(len(N)+1)
	plt.plot(X, y, color, zorder=4, label=label, lw=0.7)

plots = [1, 2, 3]
N = [2, 10, 100]
colors = ['y', 'g', 'r']

# plt.suptitle(u'Разложение в ряд по синусам')

# for plot, n, color in zip(plots, N, colors):
# 	plt.subplot(3, 1, plot)
# 	plt.title(u'Сумма %d гармоник'%(n))
# 	plot_setup()
# 	plot_function()
# 	plot_fourier(range(1, n), color)

# plt.show()

plot_setup()
plot_function()
for n, color, in zip(N, colors):
	plot_fourier(range(1, n), color)

plt.legend(loc='lower right', prop={'size':10})

# plt.show()

fig.savefig('sinus.png', dpi=800)
