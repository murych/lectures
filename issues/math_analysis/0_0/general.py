# -*- coding: UTF-8 -*-

# -------------
'''
Подключение необходимых библиотек и настройка параметров графика
'''
import matplotlib.pyplot as plt
from matplotlib import rcParams
from numpy import arange, pi, sin, cos

rcParams['figure.figsize'] = (11.69, 8.27)
rcParams['font.family'] = 'fantasy'
rcParams['font.fantasy'] = 'Arial'

fig = plt.figure()
# -------------

# -------------
def plot_setup():
	'''
	эта функция подготавливает оси: делает начало отсчета в центре,
	наносит значения, включает сетку
	'''
	plt.axis('tight')

	X_limits = (-6.5, 6.5)
	X_ticks_values = range(-6, 7)
	X_ticks_labels = [r'$%d$' % x for x in range(-6, 7)]

	y_limits = (-1.5, 1.5)
	y_ticks_values = range(-1,2,2)
	y_ticks_labels = [r'$%d$' % x for x in range(-1,2,2)]

	ax = plt.gca()
	ax.grid()

	ax.xaxis.set_ticks_position('bottom')
	ax.yaxis.set_ticks_position('left')
	ax.spines['right'].set_color('none')
	ax.spines['top'].set_color('none')
	ax.spines['bottom'].set_position(('data', 0))
	ax.spines['left'].set_position(('data', 0))
	
	plt.xlim(X_limits)
	plt.ylim(y_limits)
	plt.xticks(X_ticks_values, X_ticks_labels)
	plt.yticks(y_ticks_values, y_ticks_labels)


def plot_function():
	'''
	Эта функция выполняет построение функции и вы
	'''
	params = {'color': 'b', 'linewidth': 4, 'zorder': 3}

	X_function = [arange(-6,-4,0.1), arange(-4,-3.4,0.1), arange(-3.4,-2,0.1),
		arange(-2,0,0.1), arange(0,0.6,0.1), arange(0.6,2,0.1),
		arange(2,4,0.1), arange(4,4.6,0.1), arange(4.6,6,0.1)]

	y_function = [[0]*len(arange(-6,-4,0.1)), [1]*len(arange(-4,-3.4,0.1)), [0]*len(arange(-3.4,-2,0.1)),
        [0]*len(arange(-2,0,0.1)), [1]*len(arange(0,0.6,0.1)), [0]*len(arange(0.6,2,0.1)),
        [0]*len(arange(2,4,0.1)), [1]*len(arange(4,4.6,0.1)), [0]*len(arange(4.6,6,0.1))]

	for X, y in zip(X_function, y_function):
		plt.plot(X, y, **params)

	X_dots = [-6, -4, -4, -3.4, -3.4, -2, 0, 0, 0.6, 0.6, 2, 4, 4, 4.6, 4.6, 6]
	y_dots = [0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0]

	plt.plot(X_dots, y_dots, 'wo', zorder=3)


def plot_fourier(N, color):
	X = arange(-6, 6, 0.001)
	y = []

	a_0 = 0.3
	a_n = lambda n: sin(0.3*pi*n)/(pi*n) * cos(pi*n*x/2)
	b_n = lambda n: (-1/(pi*n)) * (cos(0.3*pi*n) - 1) * sin(pi*n*x/2)

	for x in X:
		y.append(a_0/2 + sum(map(a_n, N)) + sum(map(b_n, N)))


	label = u'Сумма %d гармоник'%(len(N)+1)
	plt.plot(X, y, color, zorder=4, label=label, lw=0.9)

plots = [1, 2, 3]
N = [2, 10, 100]
colors = ['y', 'g', 'r']

plot_setup()
plot_function()
for n, color, in zip(N, colors):
	plot_fourier(range(1, n), color)

plt.legend(loc='lower right', prop={'size':10})

fig.savefig('general.png', dpi=800)
