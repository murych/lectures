
install.packages(c("gridExtra", "ggsignif"))

library(gridExtra)
library(ggplot2)
library(ggsignif)

masses <- read.csv("masses.csv")

title <- 'Относительные массы внутренних органов, %'
x_label <- 'Группы'
y_label <- '%'
group_labels <- c("Спирт", "Интактная", "Парацетамол", "Водка")

levels(masses$group) <- c('Интактная', 'Спирт', 'Водка', 'Парацетамол', 'Тетрахлорэтан')
levels(masses$organ) <- c('Надпочечники', 'Слепая кишка', 'Почки', 'Печень', 'Селезенка')

masses_plot <- function(group_, name){
  masses_ <- subset(masses, group == 'Интактная' | group == group_)
  
  plot <- ggplot(data = masses_, aes(x = group, y = value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_fill_brewer(palette = "Set3", name = x_label) +
    theme_minimal() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    facet_wrap(~ organ, scales = 'free_y', ncol = 5) +
    scale_y_continuous(name = y_label) +
    scale_x_discrete(name = element_blank()) +
    geom_signif(comparisons = list(c("Интактная", group_)), 
                map_signif_level = TRUE, textsize = 3)
  
  ggsave(name, plot = plot, scale = 1, width = 20, height = 8, units = "cm", dpi = 600)
}

masses_plot('Спирт', 'masses_alcohol.png')
masses_plot('Водка', 'masses_vodka.png')
masses_plot('Парацетамол', 'masses_paracetamol.png')
masses_plot('Тетрахлорэтан', 'masses_ccl4.png')


alcohol  <- subset(masses, group == '1_intact' | group == '2_alcohol')
group_labels <- c("Интактная", "Спирт")
alcohol_plot <- ggplot(alcohol, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ organ, scales = 'free_y', ncol = 5)
ggsave("masses_alcohol.png", scale = 1, width = 20, height = 10, units = "cm", dpi = 300)

vodka  <- subset(masses, group == '1_intact' | group == '3_vodka')
group_labels <- c("Интактная", "Водка")
vodka_plot <- ggplot(vodka, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ organ, scales = 'free_y', ncol = 5)
ggsave("masses_vodka.png", scale = 1, width = 20, height = 10, units = "cm", dpi = 300)

paracetamol  <- subset(masses, group == '1_intact' | group == '4_paracetamol')
group_labels <- c("Интактная", "Парацетамол")
paracetamol_plot <- ggplot(paracetamol, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ organ, scales = 'free_y', ncol = 5)
ggsave("masses_paracetamol.png", scale = 1, width = 20, height = 10, units = "cm", dpi = 300)

ccl4  <- subset(masses, group == '1_intact' | group == '5_ccl4')
group_labels <- c("Интактная", "CCl4")
ccl4_plot <- ggplot(paracetamol, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ organ, scales = 'free_y', ncol = 5)
ggsave("masses_ccl4.png", scale = 1, width = 20, height = 10, units = "cm", dpi = 300)

ggplot(masses, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label)+# ,
                      #labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ organ, scales = 'free_y', ncol = 5)

ggsave("masses.png", scale = 1, width = 25, height = 10, units = "cm", dpi = 300)

wilcox.test(value ~ group, subset = masses$organ %in% 'liver' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'spleen' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'buds' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'adrenal' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'blindgut' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'liver' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'spleen' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'buds' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'adrenal' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'blindgut' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'liver' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'spleen' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'buds' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'adrenal' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'blindgut' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)
