# установка библиотеки ggplot2 для создания графиков
install.packages(c('ggplot2'))

# подключение библиотеки
library('ggplot2')

# считывание данных
df <- read.csv('date.csv')

# переименование названий групп
levels(df$group) <- c('Интактная', 'Спирт', 'Водка', 'Парацетамол', 'Тетрахлорэтан')

# вычисление медианы в группах для каждого дня
medians <- aggregate(weight ~ date + group, data = df, median)

# задание названия графика и осей
title <- 'Изменение массы тела крыс'
x_label <- 'Дата'
y_label <- 'Масса, г'

# создаем график plot_w_medians
plot_w_medians <- ggplot() +
	# добавляем на график линии, отображающие массу каждой крысы в отдельности
	# по оси x откладываются дни, по оси y - масса
    geom_line(data = df, aes(x = date, y = weight, group = number), color='gray70') +
    # управление оформлением графика
    theme_minimal() +
    theme(axis.text.x = element_text(angle = 60, hjust = 1, size = 6),
          axis.text.y = element_text(angle = 0, hjust = 1, size = 8),
          legend.position="none") +
    scale_x_discrete(expand = c(0.05, 0)) +
    # добавление линий медиан
    geom_smooth(data = medians, aes(x = date, y = weight, 
                                    group = group, color = group), size = 1.25) + 
    # разбиение графиков на отдельные группы
    facet_grid(~ group, scale = 'free_x') +
    # переименование осей
    labs(title = title, x = x_label, y = y_label)

plot_w_medians

ggsave("medians.png", plot = plot_w_medians, scale = 1, width = 35, height = 10, units = "cm", dpi = 600)

medians_plot <- function(group_, name){
  df_ <- subset(df, group == group_)
  medians_ <- aggregate(weight ~ date + group, data = df_, median)
  
  plot <- ggplot() +
    geom_line(data = df_, aes(x = date, y = weight, 
                             group = number), color = 'gray70') +
    theme_minimal() +
    theme(axis.text.x = element_text(angle = 60, hjust = 1, size = 8),
          axis.text.y = element_text(angle = 0, hjust = 1, size = 8),
          legend.position = "none") +
    scale_x_discrete(expand = c(0.05, 0)) +
    geom_smooth(data = medians_, aes(x = date, y = weight, 
                                    group = group, color = group), size = 1.25) + 
    facet_grid(~ group, scale = 'free_x') +
    labs(x = x_label, y = y_label)
  
  ggsave(name, plot = plot, scale = 1, width = 10, height = 10, units = "cm", dpi = 600)
}

medians_plot('Водка','medians_vodka.png')
medians_plot('Спирт', 'medians_alcohol.png')
medians_plot('Парацетамол', 'medians_paracetamol.png')
medians_plot('Тетрахлорэтан', 'medians_ccl4.png')
medians_plot('Интактная', 'medians_intact.png')
