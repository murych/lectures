
install.packages(c("gridExtra", "ggsignif"))

library(gridExtra)
library(ggplot2)
library(ggsignif)

masses <- read.csv("masses.csv")

title <- 'Относительные массы внутренних органов, %'
x_label <- 'Группы'
y_label <- '%'
group_labels <- c("Спирт", "CCl4", "Интактная", "Парацетамол", "Водка")

ggplot(masses, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ organ, scales = 'free_y', ncol = 5)

ggsave("masses.png", scale = 1, width = 25, height = 10, units = "cm", dpi = 300)

wilcox.test(value ~ group, subset = masses$organ %in% 'liver' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'spleen' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'buds' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'adrenal' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'blindgut' & masses$group %in% c('intact', 'paracetamol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'liver' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'spleen' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'buds' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'adrenal' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'blindgut' & masses$group %in% c('intact', 'alcohol'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'liver' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'spleen' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'buds' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'adrenal' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

wilcox.test(value ~ group, subset = masses$organ %in% 'blindgut' & masses$group %in% c('intact', 'vodka'),
            data = masses, paired = FALSE)

#---- loops ----
organs <- c('liver', 'spleen', 'buds', 'adrenal', 'blindgut')
groups <- c('paracetamol', 'alcohol', 'vodka', 'ccl4')

for (group_ in groups) {
    for (organ_ in organs) {
        p_value <- wilcox.test(value ~ group, data = masses, paired = FALSE,
                               subset = organ %in% organ_ & group %in% c('intact', group_))$p.value
        print(c(group_, organ_, p_value))
    }
}
