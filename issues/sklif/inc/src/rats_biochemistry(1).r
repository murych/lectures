
# install.packages(c("gridExtra", "ggsignif"))

library(gridExtra)
library(ggplot2)
library(ggsignif)

biochemistry <- read.csv("biochemistry.csv")

title <- 'Результаты анализов биохимии'
x_label <- 'Группы'
y_label <- 'МЕ/л'
group_labels <- c("Спирт", "Интактная", "Парацетамол", "Водка")

levels(biochemistry$group) <- c('Интактная', 'Спирт', 'Водка', 'Парацетамол', 'Тетрахлорэтан')

biochemistry_plot <- function(group_, name){
  biochem_ <- subset(biochemistry, group == 'Интактная' | group == group_)
  
  plot <- ggplot(data = biochem_, aes(x = group, y = value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_fill_brewer(palette = "Set3", name = x_label) +
    theme_minimal() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    facet_wrap(~ chem, scales = 'free_y', ncol = 4) +
    scale_y_continuous(name = y_label) +
    scale_x_discrete(name = element_blank()) +
    geom_signif(comparisons = list(c("Интактная", group_)), 
                map_signif_level = TRUE, textsize = 3)
  
  ggsave(name, plot = plot, scale = 1, width = 15, height = 10, units = "cm", dpi = 600)
}

biochemistry_plot('Спирт', 'biochemistry_alcohol.png')
biochemistry_plot('Водка', 'biochemistry_vodka.png')
biochemistry_plot('Парацетамол', 'biochemistry_paracetamol.png')
biochemistry_plot('Тетрахлорэтан', 'biochemistry_ccl4.png')

alcohol  <- subset(biochemistry, group == '1_intact' | group == '2_alcohol')
group_labels <- c("Интактная", "Спирт")
alcohol_plot <- ggplot(alcohol, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ chem, scales = 'free_y', ncol = 4)
ggsave("biochemistry_alcohol.png", scale = 1, width = 15, height = 10, units = "cm", dpi = 300)

vodka  <- subset(biochemistry, group == '1_intact' | group == '3_vodka')
group_labels <- c("Интактная", "Водка")
alcohol_plot <- ggplot(vodka, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ chem, scales = 'free_y', ncol = 4)
ggsave("biochemistry_vodka.png", scale = 1, width = 15, height = 10, units = "cm", dpi = 300)

paracetamol  <- subset(biochemistry, group == '1_intact' | group == '4_paracetamol')
group_labels <- c("Интактная", "Парацетамол")
alcohol_plot <- ggplot(paracetamol, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ chem, scales = 'free_y', ncol = 4)
ggsave("biochemistry_paracetamol.png", scale = 1, width = 15, height = 10, units = "cm", dpi = 300)

ccl4  <- subset(biochemistry, group == '1_intact' | group == '5_ccl4')
group_labels <- c("Интактная", "CCl4")
alcohol_plot <- ggplot(ccl4, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ chem, scales = 'free_y', ncol = 4)
ggsave("biochemistry_ccl4.png", scale = 1, width = 15, height = 10, units = "cm", dpi = 300)

ggplot(biochemistry, aes(group, value, fill = group)) +
    geom_boxplot() +
    stat_boxplot(geom = "errorbar", width = 0.5) +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 0.5, size = 8)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ chem, scales = 'free_y', ncol = 4)

ggsave("biochemistry.png", scale = 1, width = 25, height = 10, units = "cm", dpi = 300)

wilcox.test(value ~ group, subset = chem %in% 'ALP' & group %in% c('intact', 'paracetamol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'ALT' & group %in% c('intact', 'paracetamol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'AST' & group %in% c('intact', 'paracetamol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'LDH' & group %in% c('intact', 'paracetamol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'ALP' & group %in% c('intact', 'alcohol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'ALT' & group %in% c('intact', 'alcohol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'AST' & group %in% c('intact', 'alcohol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'LDH' & group %in% c('intact', 'alcohol'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'ALP' & group %in% c('intact', 'vodka'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'ALT' & group %in% c('intact', 'vodka'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'AST' & group %in% c('intact', 'vodka'),
            data = biochemistry, paired = FALSE)

wilcox.test(value ~ group, subset = chem %in% 'LDH' & group %in% c('intact', 'vodka'),
            data = biochemistry, paired = FALSE)

ferments <- c('ALP', 'ALT', 'AST', 'LDH')
groups <- c('paracetamol', 'alcohol', 'vodka')

for(group_ in groups){
    for(ferment_ in ferments){
        p_value <- wilcox.test(value ~ group, data = biochemistry, paired = FALSE,
                               subset = chem %in% ferment_ & group %in% c('intact', group_))$p.value
        print(c(group_, ferment_, p_value))
    }
}
