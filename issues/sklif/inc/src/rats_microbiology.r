
# install.packages(c("gridExtra", "ggsignif"))

library(gridExtra)
library(ggplot2)
library(ggsignif)

microbiology <- read.csv("microbiology.csv")

microbiology <- subset(microbiology, 
                       bacteria != 'mushrooms' &
                       bacteria != 'st.aureus' &
                       bacteria != 'e.coli ehec' &
                       bacteria != 'proteus sp.' &
                       bacteria != 'staphylococcus sp.' &
                       bacteria != 'mushrooms' &
                       bacteria != 'lactobacilli' &
                       bacteria != 'bifidobacteria' &
                       bacteria != 'bacillus spp.')

# microbiology <- subset(microbiology, group != 'ccl4')

microbiology <- aggregate(value ~ bacteria + group,
                          data = microbiology,
                          FUN = median)

title <- 'Результаты микробиологического анализа, медианы'
x_label <- 'Группы'
y_label <- 'Количество, log(10^n)'
group_labels <- c("Спирт", "CCl4", "Интактная", "Парацетамол", "Водка")

ggplot(microbiology, aes(group, value, fill = group)) +
    geom_bar(stat = "identity") +
    scale_y_log10() +
    scale_x_discrete() +
    scale_fill_brewer(palette = "Set3",
                      name = x_label,
                      labels = group_labels) +
    theme_bw() +
    theme(axis.text.x = element_blank(),
          axis.text.y = element_text(angle = 60, hjust = 1)) +
    labs(title = title,
         x = element_blank(),
         y = y_label) +
    facet_wrap(~ bacteria, ncol = 5)

ggsave("microbiology.png", scale = 1, 
       width = 25, height = 10, 
       units = "cm", dpi = 300)

groups <- c('paracetamol', 'alcohol', 'vodka', 'ccl4')
bacterias <- c('e.coli','e.coli ehec','enterococcus sp.',
               'proteus sp.','enterobacter sp.',
               'klebsiella spp.','pseudomonas spp.')

# bacterias <- c('e.coli','e.coli ehec','enterococcus sp.',
#                'proteus sp.','enterobacter sp.','klebsiella spp.',
#                'pseudomonas spp.','staphylococcus sp.','st.aureus',
#                'mushrooms','lactobacilli','bifidobacteria','bacillus spp.')

sink('output.txt')

for (group_ in groups) {
    for (bacteria_ in bacterias) {
        p_value <- try(wilcox.test(value ~ group,
                               data = microbiology,
                               paired = FALSE,
                               subset = bacteria %in% bacteria_ & 
                                   group %in% c('intact', group_))$p.value, 
                       silent = T)
        print(c(group_, bacteria_, p_value))
    }
}

sink()
