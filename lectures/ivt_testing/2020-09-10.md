## Физический признак

По физическому признаку испытания делятся на:

1. механические испытания;
2. электрические испытания;
3. климатические испытания;
4. испытания на надежность;
5. параметрические испытания.

### Механические испытания

Механические испытания проводятся с целью проверки устойчивости изделия к действию внешних возмущающих механических факторов.

В ходе этих испытаний определяют:

1. вибропрочность;
2. ударопрочность;
3. виброустойчивость;
4. удароустойчивость;
5. устойчивость к линейным и круговым нагрузкам;
6. устойчивость к транспортной тряске.

Под _вибропрочностью_ понимается способность изделия выполнять свои функции в соответствии с требованиями ТУ _после_ прекращения действия внешних возмущающих факторов.

Эти испытания проводятся, как правило, путем воздействия на изделие синусоидальной вибрацией фиксированной частоты.

После завершения таких испытаний изделие должно работать.

Под _виброустойчивостью_ понимается способность изделия выполнять свои функции во время действия внешних возмущающих факторов.

Эти испытания проводятся путем воздействия на изделие синусоидальной вибрации с плавающей (качающейся) частотой, т.е. частота плавно изменяется в некоем диапазоне, обычно от 0 до 1500 Гц.

В ходе этих испытаний определяется _собственная_ и _резонансная_ частоты изделия.

#### Вибростенд

Для проведения механических испытаний используются вибростенды.

<img src="https://i.ibb.co/7gpS1jz/firefox-7-Sg87r-HBS0.png" alt="Схема вибростенда"  />

Изделие (1) установлено на вибростоле (2) и через демпферы (3) подвешено над основанием (4).
Шток (5) вибростола через подшипник (6) опирается на эксцентрик (7).
Эксцентрик при своем вращении сообщает штоку возвратно-поступательные движения.

Обычно применяются амплитуды до 1 мм и частоты до 50 Гц.

#### Электродинамический стенд

![Схема электродинамического стенда](https://i.ibb.co/LPDzTcP/image.png)

Объект испытаний (1) устанавливается на вибростол (2), который на демпферах (3) подвешен над электромагнитной катушкой (4).
При подаче напряжения? возникает эффект соленоида, при условии, что шток (5) вибростола изготовлен из ферромагнетика, т.е. шток выталкивается вверх.
На объекте установлен вибродатчик (6), обычно, на пьезоэффекте.
Сигнал с него поступает на усилитель (7), и приходит на сравнитель (8)?, где реальный сигнал сравнивается с заданным.
Сигнал рассогласования поступает на корректор (9), который на звуковой генератор (10) отправляет сигнал на изменение выходной частоты.

?Тяговое усилие такого стенда до 1 кг. Частота вибрации от 20 Гц до 20 кГц. Амплитуда – до 0.1 мм.