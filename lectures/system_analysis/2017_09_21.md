# Управление системой

_Управление системой_ -- выполнение целенаправленных воздействий на систему с целью обеспечения требуемого течения процессов в системе или с целью обеспечения требуемого изменения её состояния.

К числу основных функций и задач управления относятся:

- организация системы -- выделение подсистем, описание их взаимодействия и структуры систем;
- прогнозирование поведения системы -- исследование её будущего состояния;
- планирование -- координация ресурсов, элементов, подсистем и структуры системы, необходимых для обеспечения цели функционирования системы;
- регулирование -- адаптация системы к изменению внешней среды.

В процессе управления системой участвуют два объекта -- устройство управления (управляющая система) и объект управления (управляемая система).

![Система без обратной связи](pics/2017_09_21_01.png)
![Система с обратной связью](pics/2017_09_21_02.png)

## Обратная связь

_Обратная связь_ -- соединение между выходом и входом системы, осуществляемое либо непосредственно, либо через другие элементы системы.

_Чувствительный элемент_ содержит в своем составе измерительные средства, которые именуются _датчиками_ или _сенсорами_. Назначение чувствительного элемента -- это измерение и преобразование сигналов, несущих информацию о требуемом и фактическом значении параметров, а в некоторых случаях -- и о внешних возмущениях.

_Вычислительное устройство_ реализует программу управления. В простейшем случае такая программа управления реализуется путем сравнения заданного значения параметра с его фактическим значением (чаще всего -- операция вычитания).

_Исполнительное устройство_ предназначено для непосредственного воздействия на объект управления для изменения его состояния по обратным сигналам, формируемым вычислительным устройством.

Основными функциями обратной связи являются

- противодействие выходу параметров системы за установленные пределы;
- компенсация возмущений и поддержание состояния устойчивого равновесия системы;
- выработка управляющих воздействий на объект управления по плохо формализуемому закону, т.е. таких управляющих воздействий, которые невозможно описать с помощью аналитических выражений.

Системы подразделяют на:

- устойчивые системы -- состояние, определяющее неизменность её существенных переменных;
- неустойчивые системы -- состояние, при котором система, организованная для выполнения определенных функций, перестает их выполнять под влиянием каких-то причин.

Способность системы оставаться устойчивой через изменение своей структуры и поведения называется ультрастабильностью.

Особенность системы управления человеческим организмом -- это ее сложная иерархическая структура, содержащая 6 уровней иерархии:

- Сознание -- наиболее сложная подсистема управления поведением организма. На этом уровне решаются интеллектуальные задачи. Также сознание -- это подсистема управления, обеспечивающая взаимодействие человека с внешней средой и её явлениями. Кроме того, сознание включает способность человека формировать цель и способы взаимодействия с объектами внешней среды;
- Подсознание -- подсистема управления, контролирующая поведение человека в уже известных, ранее встречавшихся ситуациях. Наличие этой подсистемы разгружает верхний уровень системы от управления обычными, рутинными операциями: бег, ходьба, координация движений и т.д. Этот уровень реализуется с помощью ЦНС с привлечением информационных свойств головного мозга.
- Нервно-соматический уровень управления -- подсистема обеспечивает безусловную рефлекторную деятельность: глотательные движения, регулярные движения мускулатуры и т.д.
- Уровень функциональных систем -- этот уровень управления включает управление функциональными системами организма и органами, а также обменом веществ. Осуществляется, главным образом, вегетативной нервной системой. Например: сердечные сокращения. Обладает автономностью, но необходимо взаимодействие с ЦНС;
- Гормональный уровень управления -- химический способ управления, осуществляемый эндокринной системой посредством гормонов. Гормоны -- специальные сигнальные вещества, вырабатываемые железами внутренней секреции и разносимые кровотоком;
- Клеточный уровень управления -- управление на межклеточном уровне, осуществляется химически с помощью специальных белков -- ферментов. Управление внутри клетки реализуется электрическими и химическими процессами. Клетки воспринимают информацию, которую несут химические, электрические, магнитные, электромагнитные и световые сигналы. В клетке имеются система управления и средства связи, позволяющие ускорять синтез веществ или прекращать его. Кроме того, клетки обладают способностью размножасться делением для воспроизведения себе подобных.

<!-- % рисунок "`система управления человеческим организмом"' -->

Входная информация -- химические, физические, электрические, магнитные, электромагнитные, световые, тепловые и другие типы воздействий. Каждая подсистема также имеет собственную внутреннюю сложную систему управления.

Система управления в свою очередь также входит в большую систему функционирования организма?

<!-- % рисунок "`большая система управления"' -->

Интерфейс организма -- система нервов кровеносных сосудов, канал передачи электромагнитных сигналов.

Система восприятия информации -- служит для получения и преобразования информации из внешней среды и информации о внутреннем состоянии организма.

Исполнительная система организма -- костно-мышечная система и мышцы внутренних органов. Руки, ноги, мышцы скелета обеспечивают воздействие на внешнюю среду при выполнении различных движений. Мышцы внутренних органов позволяют изменять внутреннее состояние органов для обеспечения различных режимов работы организма.

Система диагностики и самовосстановления -- обеспечивает слежение за нормальным функционированием органов и систем, самовосстановление организма в случае болезни. К этим системам можно отнести иммунную и лимфатическую системы.

Система воспроизводства -- объединяет органы размножения, позволяющие воспроизводить аналогичные организмы.

Система жизнеобеспечения -- все системы обмена с внешней средой: дыхательная, пищеварительная. Также она отвечает за обмен с внешней средой и между внутренними органами.