program fff;
uses crt;
var s, t, y : real;

function f(a, b, c : real): real;
begin
    f := (2 * a - b + sin(c)) / (5 + abs(c))
end;

BEGIN
readln(s, t);

y := f(t - 2 * s, 1, 17) + f(2.2, t, s+ t);

writeln('y= ', y);

readln;
END.