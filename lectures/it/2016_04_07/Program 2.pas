program n8;

uses
  crt;

type
  zap = record
    group: string[10]; //[10] - ограничение на длину строки
    surname: string[15];
    name: string[10];
    mark: array[1..5] of integer;
  end;

var
  file_in, file_out: text;
  l, k: integer;
  z: zap;

BEGIN
  ClrScr;
  
  assign(file_in, 'c:\input.txt');
  reset(file_in);
  
  assign(file_out, 'c:\output.txt');
  rewrite(file_out);
  
  writeln('Фамилии студентов-задолжников');
  writeln(file_out, '......');
  
  repeat
    read(file_in, z.group, z.surname, z.name);
    for i := 1 to 5 do read(file_in, z.mark[i]);
    readln(file_in);
    
    for k := 1 to 5 do
      if z.mark[k] < 3 then begin
        write(file_out, z.group, z.name[1], '.', z.surname);
        writeln(file_out);
      end;
    
  until eof(file_in);
  close(file_in);
  close(file_out);
  readln;
  
END.