program P1;

uses
  crt;

label 10;

var
  a, b, h, x, y: real;

begin
  clrscr;
  writeln('Enter a,b,h');
  read(a, b, h);
  x := a;
  10:	y := 2 * x + 3;	{f(x)}
  writeln('x = ', x:4:2, ' y = ', y);
  x := x + h;
  if x <= b then
    goto 10;
  readln;
end.