program P3;

uses
  crt;

var
  a, b, h, x, y: real;

begin
  clrscr;
  readln(a, b, h);
  x := a;
  while x <= b do
  begin
    y := f(x);
    writeln('x = ', x:4:2, ' y = ', y);
    x := x + h;
  end;
  readln;
end.