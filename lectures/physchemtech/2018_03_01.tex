\paragraph{Главные напряжения}

Используя полученные формулы и поворачивая наклонную площадку в разных 
направлениях, можно найти три площадки, на которых действует только 
нормальное напряжение, а касательные напряжения отсутствуют. Эти площадки 
называются \emph{главными}. Нормальные напряжения $\sigma_1$, $\sigma_2$, $\sigma_3$,
($\sigma_1 \geq \sigma_2 \geq \sigma_3$) действующие на этих площадках 
называются \emph{главными напряжениями}. Оси координат $x_1$, $y_1$, $z_1$ 
называются главными осями. Направления, по которым действуют эти напряжения, 
называются \emph{главными направлениями}.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\textwidth]{pics/9_21}
	\caption{}
	\label{fig:9_21}
\end{figure}

Если взять тензор напряжения в главных осях, то он упрощается и принимает вид:
\[
	T_\sigma = \left( \begin{matrix}
					\sigma_1 & 0 & 0\\
					0 & \sigma_2 & 0\\
					0 & 0 & \sigma_{3}
				\end{matrix} \right),~\sigma_1 \geq \sigma_2 \geq \sigma_3
\]

Использование главных осей позволяет значительно упростить расчеты по определению полного напряжения на наклонной площадке. Из формул 9.4 и 9.7 находим:

\begin{equation}
\begin{cases}
	\sigma_{ix} = \sigma_1 l;~\sigma_{iy} = \sigma_2 m;~\sigma_{iz} = \sigma_3 n;\\
	\sigma_i^2 = \sigma_1^2 l^2 + \sigma_2^2 m^2 + \sigma_3^2 n^2;\\
	\sigma_\nu = \sigma_1 l^2 + \sigma_2 m^2 + \sigma_3 n^2;\\
	\tau^2 = \sigma_1^2 l^2 + \sigma_2^2 m^2 + \sigma_3^2 n^2 - \left(\sigma_1 l^2 + \sigma_2 m^2 + \sigma_3 n^2\right)^2.
\end{cases}
\label{eq:9_9}
\end{equation}

Используя первую формулу 9.9 и соотношение $l^2 + m^2 + n^2 = 1$, получаем 
уравнение эллипсоида, отнесенное к центру и главным осям:
\begin{equation}
	\frac{\sigma_{ix}^2}{\sigma_1^2} + \frac{\sigma_{iy}^2}{\sigma_2^2} + 
		\frac{\sigma_{iz}^2}{\sigma_3^2} = 1.
\end{equation}

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.6\textwidth]{pics/9_22}
	\caption{}
	\label{fig:9_22}
\end{figure}

Полуоси эллипсоида соответственно равны трем главным напряжениям.
Любой радиус эллипсоида представляет собой полное напряжение на наклонной 
площадок, а проекции этого радиуса на оси координат равны составляющим $\sigma_{ix}$,
$\sigma_{iy}$, $\sigma_{iz}$. Т.е. напряженное состояние в точке имеет наглядную 
геометрическую интерпретацию в виде эллипсоида напряжений.

Определим теперь главные нормальные напряжения при заданном тензоре 
напряжений в произвольной системе координат. Выберем наклонную площадку с 
направляющими косинусами $l$, $m$, $n$, на которой действует только главное 
напряжение $\sigma$. В данном случае $\sigma_{ix} = \sigma l$, $\sigma_{iy} = 
\sigma m$, $\sigma_{iz} = \sigma n$. Подставим эти выражения в 9.4, получаем:

\begin{equation}
	\begin{cases}
		(\sigma_x - \sigma) l + \tau_{xy} m + \tau_{xz} n = 0; \\
		\tau_{yx} l + (\sigma_y - \sigma) m + \tau_{yz} n = 0; \\
		\tau_{zx} l + \tau_{zy} m + (\sigma_z - \sigma) n = 0.		
	\end{cases}
\end{equation}

Необходимо найти значения $l$, $m$, $n$, при которых данная система 
выполняется. Система уравнений однородна, сами косинусы не могут быть 
одновременно равны нулю. Поэтому определитель системы должен быть равен нулю:

\[
	\qty|
		\begin{matrix}
			\sigma_x - \sigma & \tau_{xy} & \tau_{xz} \\
			\tau_{yx} & \sigma_y - \sigma & \tau_{yz} \\
			\tau_{zx} & \tau_{zy} & \sigma_z - \sigma 
		\end{matrix}
	| = 0
\]

Вычисляя определитель, можно получить кубическое уравнение относительно 
$\sigma$, корни которого $\sigma_1$, ... являются главными напряжениями:

\[
	\sigma^2 - I_1 \sigma^2 + I_2 \sigma - I_3 = 0
\]
где
\begin{equation}
	\begin{cases}
		I_1 = \sigma_x + \sigma_y + \sigma_z;\\
		I_2 = \sigma_x \sigma_y + \sigma_y \sigma_z + \sigma_z \sigma_x - \tau_{xy}^2 
			- \tau_{yx}^2 - \tau_{zx}^2;\\
		I_3 = \sigma_x \sigma_y \sigma_z - 2 \tau_{xy} \tau_{yz} \tau_{zx} 
			- \sigma_x \tau_{yz}^2 + \sigma_y \tau_{zx}^2 - \sigma_z \tau_{xy}^2.
	\end{cases}
	\label{eq:9_10}
\end{equation}

Величины I называют соответственно линейным, квадратичным и кубичным 
инвариантами тензора напряжений, т.к. они не зависят от выбора системы 
координат.

\[
	\begin{cases}
		I_1 = \sigma_1 + \sigma_2 + \sigma_3; \\
		I_2 = \sigma_1 \sigma_2 + \sigma_2 \sigma_3 + \sigma_3 \sigma_1; \\
		I_3 = \sigma_1 \sigma_2 \sigma_3.
	\end{cases}
\]

\paragraph{Диаграмма Мора. Главные касательные напряжения.} В теории ПД представляют интерес максимальные касательные напряжения и положения площадок, на которых они действуют.

Решая систему \ref{eq:9_9} относительно косинусов углов, можно получить:
\begin{equation}
	\begin{cases}
		l^2 = \frac{(\sigma_2 - \sigma_\nu) (\sigma_3 - \sigma_\nu) + \tau^2}{(\sigma_2 - \sigma_1) (\sigma_3 - \sigma_1)}; \\
		m^2 = \frac{(\sigma_3 - \sigma_\nu) (\sigma_1 - \sigma_\nu) + \tau^2}{(\sigma_3 - \sigma_2) (\sigma_1 - \sigma_2)}; \\
		n^2 = \frac{(\sigma_1 - \sigma_\nu) (\sigma_2 - \sigma_\nu) + \tau^2}{(\sigma_1 - \sigma_3) (\sigma_2 - \sigma_3)}.
	\end{cases}
	\label{eq:9_11}
\end{equation}

Поскольку $\sigma_1 > \sigma_2 > \sigma_3$, знаменатели правых частей первого и 
третьего уравнений \ref{eq:9_11} положительны, а знаменатель второго -- отрицателен,
можно записать:
\begin{equation}
	\begin{cases}
		(\sigma_2 - \sigma_\nu)(\sigma_3 - \sigma_\nu) + \tau^2 \geq 0; \\
		(\sigma_3 - \sigma_\nu)(\sigma_1 - \sigma_\nu) + \tau^2 \leq 0; \\
		(\sigma_1 - \sigma_\nu)(\sigma_2 - \sigma_\nu) + \tau^2 \geq 0.		
	\end{cases}
	\label{eq:9_12}
\end{equation}

Выражения \ref{eq:9_12} определяют область допустимых значений $\sigma$ и $\tau$,
при равенстве левой части нулю эти значения предельные. В этом случае неравенства
превращаются в уравнения, которые можно преобразовать в уравнения окружности, например:
\[
	\qty(\sigma_\nu - \frac{\sigma_2 + \sigma_3}{2})^2 + \tau^2 = 
		\frac{\qty(\sigma_2 + \sigma_3)^2}{2}
\]

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.6\textwidth]{pics/9_23}
	\caption{}
	\label{fig:9_23}
\end{figure}

Построим в осях $\sigma_\nu$ и $\tau$ окружности с радиусами $(\sigma_2 - \sigma_3)/2$ 
и т.д. с центрами на оси $\sigma_\nu$ на расстояниях $(\sigma_2 + \sigma_3)/2$ и т.д.
от начала координат, определяем границы областей $\sigma_\nu$ и $\tau$ 
(рис. \ref{fig:9_23}). На диаграмме видны экстремальные значения нормальных напряжений 
и соответствующие им касательные напряжения. Экстремальные значения касательных напряжений:
\begin{equation}
	\begin{cases}
		\tau_{12} = \pm \frac{\sigma_1 - \sigma_2}{2};\\
		\tau_{23} = \pm \frac{\sigma_2 - \sigma_3}{2};\\
		\tau_{31} = \pm \frac{\sigma_3 - \sigma_1}{2}.
	\end{cases}
	\label{eq:9_13}
\end{equation}

Эти значения называются главными касательными напряжениями. Очевидно, что 
наибольшей по модулю из них является величина $\tau_{31}$.

Подставляя эти значения и значения главных касательных напряжений \ref{eq:9_13}
в уравнения \ref{eq:9_11}, получаем значения направляющих косинусов, 
определяющих положение площадок, на которых действуют главные касательные напряжения:
\begin{equation}
	\begin{matrix}
		~	&	\tau_{12}		&	\tau_{23}	&	\tau_{31}\\
		l 	&	0				& \pm\sqrt{2}/2	&	\pm\sqrt{2}/2\\
		m 	&	\pm\sqrt{2}/2	& 0				&	\pm\sqrt{2}/2\\
		l 	&	\pm\sqrt{2}/2	& \pm\sqrt{2}/2	&	0\\
	\end{matrix}
\end{equation}

Таким образом, главные касательные напряжения действуют попарно по шести площадкам,
которые расположены под углом \SI{45}{\degree} к двум осям и параллельны третьей оси.

\paragraph{Октаэдрические напряжения}. Для теории ПД важно знать также 
напряжения, действующие на равнонаклоненных площадках, т.е. на площадках, 
которые образуют октаэдр. Направляющие косинусы таких площадок равны $l = m = 
n = \pm\frac{1}{\sqrt{3}}$. Подставляя эти значения во второе и третье 
уравнения \ref{eq:9_9}, получаем выражения для полного и нормального октаэдрических 
напряжений:
\begin{equation}
	\sigma_{i,\text{окт}}^2 = \frac13 \qty(\sigma_1^2 + \sigma_2^2 + \sigma_3^2);
	\label{eq:9_14}
\end{equation}
\begin{equation}
	\sigma_{\nu,\text{окт}} = \frac13 \qty(\sigma_1 + \sigma_2 + \sigma_3) = \sigma_c
	\label{eq:9_15}
\end{equation}

Величину $\sigma_c$ называют также средним или гидростатическим давлением.
\begin{equation}
	\sigma_c = \frac13 \qty(\sigma_x + \sigma_y + \sigma_z)
	\label{eq:9_15_}
\end{equation}

Касательное октаэдрическое напряжение определяем из четвертого уравнения \ref{eq:9_9}
с учетом \ref{eq:9_14} и \ref{eq:9_15}:
\begin{equation}
	\tau_\text{окт} = \frac13 \sqrt{(\sigma_1 - \sigma_2)^2 + (\sigma_2 - \sigma
_3)^2 + (\sigma_3 - \sigma_1)^2}
	\label{eq:9_16}
\end{equation}
или через инварианты тензора напряжений:
\[
	\tau_\text{окт} = \frac13 \sqrt{2(I_1^2 - 3I_2)}
\]

С учетом \ref{eq:9_13} можно написать также
\begin{equation}
	\tau_\text{окт} = \frac28 \sqrt{\tau_{12}^2 + \tau_{23}^2 + \tau_{31}^2}
\end{equation}

Если известны напряжения на произвольных площадках, то, используя \ref{eq:9_10} 
$\tau_\text{окт}$ можно найти как
\begin{equation}
	\tau_\text{окт} = \frac13 \sqrt{\qty(\sigma_x - \sigma_y)^2 + 
		\qty(\sigma_y - \sigma_z)^2  + 
		\qty(\sigma_z - \sigma_x)^2  + 
		6 \qty(\tau_{xy}^2 + \tau_{yz}^2 + \tau_{zx}^2) }
	\label{eq:9_17}
\end{equation}

Величину $\bar\sigma_i = (3 \tau_\text{окт}) / \sqrt2$ или
\begin{equation}
	\bar\tau_\text{окт} = \frac{1}{\sqrt2} \sqrt{\qty(\sigma_x - \sigma_y)^2 + 
		\qty(\sigma_y - \sigma_z)^2  + 
		\qty(\sigma_z - \sigma_x)^2  + 
		6 \qty(\tau_{xy}^2 + \tau_{yz}^2 + \tau_{zx}^2) }
	\label{eq:9_18}
\end{equation}
называют интенсивностью напряжений или обобщенным напряжением. Через главные 
напряжения $\bar\sigma_i$ выражается как
\begin{equation}
	\bar\tau_\text{окт} = \frac{1}{\sqrt2} \sqrt{\qty(\sigma_1 - \sigma_2)^2 + 
		\qty(\sigma_2 - \sigma_3)^2  + 
		\qty(\sigma_3 - \sigma_1)^2 }
	\label{eq:9_19}
\end{equation}

Используется также интенсивность касательных напряжений:
\begin{equation}
	T = \frac{\bar\sigma_i}{\sqrt3}
	\label{eq:9_20}
\end{equation}

\paragraph{Шаровой тензор и тензор-девиатор напряжений.} Если тело поместить 
в равномерно сжимающее состояние (например, в воду) то её напряженное 
состояние характеризуется тремя равными друг другу главными напряжениями, 
каждое из которых равно среднему или гидростатическому. В этом случае 
эллипсоид напряжений является шаром, а тензор \ref{eq:9_8} переходит в величину
\begin{equation}
	(T_\sigma)_\text{ш} = \left(\begin{matrix}
		\sigma_c & 0 & 0\\
		0 & \sigma_c & 0\\
		0 & 0 & \sigma_c
	\end{matrix}\right)
	\label{eq:9_20_}
\end{equation}
которая называется \emph{шаровым тензором}. Шаровому тензору соответствует 
изменение объема без изменения формы. Если главные напряжения не равны друг 
другу, то помимо изменения объема изменяется норма тела. Чистое изменение 
формы без изменения объема тела описывается тензором, называемым тензором-
девиатором напряжений $D_\sigma$. Все компоненты общего тензора напряжений, 
учитывающего изменение и объема, и формы, являются суммами компонентов 
обоих указанных тензоров, или обобщенно:
\[
	T_\sigma = (T_\sigma)_\text{ш} + D_\sigma.
\]

Тензор-девиатор можно найти как:
\begin{equation}
	D_\sigma = T_\sigma - \qty(T_\sigma)_\text{ш} = \qty(
	\begin{matrix}
		\sigma_x - \sigma_c & \tau_{xy} & \tau_{xz} \\
		\tau_{yx} & \sigma_y - \sigma_c & \tau_{yz} \\
		\tau_{xz} & \tau_{zy} & \sigma_z - \sigma_c
	\end{matrix}
	)
	\label{eq:9_21}
\end{equation}

\paragraph{Схемы напряженного состояния.} При обработке давлением стремятся 
заготовку располагать так, чтобы действовало только нормальное напряжение, а 
касательные равны нулю. Тогда получаем следующие схемы напряженного 
состояния, предложенные С.И. Губкиным: линейные (а); плоские (б), объемные (в):

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.9\textwidth]{pics/9_25}
	\caption{}
	\label{fig:9_25}
\end{figure}

Наиболее благоприятна в отношении пластичности -- схема 4, а наименее -- схема 1.

\paragraph{Дифференциальные уравнения равновесия.} Вновь рассмотрим 
элементарный параллелепипед.

\begin{equation}
	\sigma_x + \frac{\partial \sigma_x}{\partial x}dx, \quad \tau_{yx} + \frac{\partial \tau_{xy}}{\partial x}dx, \quad \tau_{zx} + \frac{\partial \tau_{zx}}{\partial x}dx.
\end{equation}

Аналогичные соотношения можно написать и для двух других граней. При 
равновесии сумма проекций всех сил, например, на ось $Ox$ равна нулю:

....

Сокращаем и делим на элементы объема $dx$, $dy$, $dz$, получаем:

\begin{equation}
	\begin{cases}
		\frac{\partial \sigma_x}{\partial x} + \frac{\partial \tau_{xy}}{\partial y} + \frac{\partial \tau_{xz}}{\partial z} = 0; \\
		\frac{\partial \tau_{yx}}{\partial x} + \frac{\partial \sigma_y}{\partial x} + \frac{\partial \sigma_x}{\partial x} = 0; \\
		\frac{\partial \sigma_x}{\partial x} + \frac{\partial \sigma_x}{\partial x} + \frac{\partial \sigma_x}{\partial x} = 0;		
	\end{cases}
\end{equation}
% доделать1!!

\paragraph{Деформированное состояние в точке.} Напряжения, действующие в 
твердом теле, вызывают его деформацию, т.е. изменение объема и формы. В 
теории деформации рассматривают малые деформации, чтобы исследовать изменения 
от точки к точке.

Деформация характеризуется абсолютными и относительными величинами. Пусть 
точка $A$ тела смещается из положения $A(x, y, z)$ в положение $A'(x', y', z')$
(рис. 9.27). Вектор $\vec{AA'}$ называется перемещением $\vec{U}$, а его 
проекция на оси координат -- проекциями перемещения $U_x$, $U_y$, $U_z$.
% рис.9.27

Возьмем теперь две точки тела 1 и 2, лежащие на малом расстоянии и вначале 
рассмотрим только проекцию отрезка на ось $x$. Пусть проекции перемещения 
точек равны $U_{1x} = x_1' - x_1; ~ U_{2x} = x_2' - x_2$. Тогда 
$\Delta U = U_{2x} - U_{1x}$ -- разность перемещений концов отрезка, а
\begin{equation}
	\varepsilon_x = \lim_{x_2 \to x_1} \frac{U_{2x} - U_{1x}}{x_2 - x_1} = \lim_
{\Delta x \to 0} \frac{\Delta U_x}{\Delta x} = \frac{\partial U_x}{\partial x}
\end{equation}
называется относительным удлинением или линейной деформацией по оси $x$. 
Если отрезок 1-2 удлиняется ...........

\paragraph{Пространственная деформация тела.} Рассмотрим деформацию 
элементарного параллелепипеда. В процессе деформации его ребра изменяют свои 
размера, т.е. получают перемещения и относительные удлинения, а грани 
искажаются: из прямоугольников превращаются в параллелограммы, т.е. кроме 
линейных возникают также угловые деформации (повороты).
% рис 9.29

\begin{equation}
	\begin{dcases}
		\varepsilon_x = \pdv{U_x}{x}; \quad \gamma_{xy} = \pdv{U_x}{x} + \pdv{U_y}{y}; \\
		\varepsilon_y = \pdv{U_y}{y}; \quad \gamma_{yz} = \pdv{U_y}{y} + \pdv{U_z}{z}; \\
		\varepsilon_z = \pdv{U_z}{z}; \quad \gamma_{zx} = \pdv{U_z}{z} + \pdv{U_x}{x},
	\end{dcases}
	\label{eq:9_23}
\end{equation}
где $\gamma_{xy}$, $\gamma_{yz}$, $\gamma_{zx}$ -- угловые деформации (радианы), или
относительные сдвиги между соответствующими осями. $\gamma_{ij}$ считаются 
положительными, если уменьшение прямого угла происходит между положительными
направлениями осей координат. Для удобства расчета берут только половинные значения:
$1/2 \gamma_{xy}$ и т.д., поскольку от одного значения к другому можно перейти без
деформации, имея только напряженное состояние.

\paragraph{Тензор деформаций.}

\begin{equation}
	T_\varepsilon = \qty(\begin{matrix}
		\varepsilon_x 	& 1/2 ~ \gamma_{yx}	& 1/2 ~ \gamma_{zx} \\
		1/2 ~ \gamma_{xy}	& \varepsilon_y	& 1/2 ~ \gamma_{zy} \\
		1/2 ~ \gamma_{xz} 	& 1/2 ~ \gamma_{yz}	& \varepsilon_z
	\end{matrix})
	\label{eq:9_24}
\end{equation}

Как и в теории напряжений, в теории деформаций пользуются понятиями главных 
деформаций $\varepsilon_1$, $\varepsilon_2$ и $\varepsilon_3$:
\[
	T_\varepsilon = \left(\begin{matrix}
		\varepsilon_x 	& 0	& 0 \\
		0	& \varepsilon_y	& 0 \\
		0 	& 0	& \varepsilon_z
	\end{matrix}\right)
\]

Тензор деформаций как и тензор напряжений можно разложить на шаровой тензор 
$\qty(T_\epsilon)_\text{ш}$ и тензор-девиатор $D_\varepsilon$ деформаций:

\begin{equation}
	(T_\varepsilon)_\text{ш} = \left(\begin{matrix}
		\varepsilon_c 	& 0	& 0 \\
		0	& \varepsilon_c	& 0 \\
		0 	& 0	& \varepsilon_c
	\end{matrix}\right)
	\label{eq:9_25}
\end{equation}

\begin{equation}
	D_\varepsilon = \qty(\begin{matrix}
		\varepsilon_x - \varepsilon_c 	& 1/2 ~ \gamma_{yx}	& 1/2 ~ \gamma_{zx} \\
		1/2 ~ \gamma_{xy}	& \varepsilon_y - \varepsilon_c	& 1/2 ~ \gamma_{zy} \\
		1/2 ~ \gamma_{xz} 	& 1/2 ~ \gamma_{yz}	& \varepsilon_z - \varepsilon_c
	\end{matrix}),
	\label{eq:9_26}
\end{equation}
где $\varepsilon_c$ -- средняя линейная деформация
\begin{equation}
	\varepsilon_c = \frac13 \theta = \frac{\varepsilon_x + \varepsilon_y + \varepsilon_z}{3}
	\label{eq:9_27}
\end{equation}

Как и в теории напряжений, вводятся понятия интенсивности деформаций
\begin{equation}
	\varepsilon_i = \frac{\sqrt2}{3} \sqrt{\qty(\varepsilon_x - \varepsilon_y)^2 + 
		\qty(\varepsilon_y - \varepsilon_z)^2 + \qty(\varepsilon_z - \varepsilon_x)^2 +
		\frac32 \qty(\gamma_{xy}^2 + \gamma_{yz}^2 + \gamma_{zx}^2 )}
	\label{eq:9_28}
\end{equation}
которая в главных осях имеет вид
\[
	\varepsilon_i = \frac{\sqrt2}{3} \sqrt{\qty(\varepsilon_1 - \varepsilon_2)^2 + 
		\qty(\varepsilon_2 - \varepsilon_3)^2 + \qty(\varepsilon_3 - \varepsilon_1)^2}
\]
а также интенсивности деформации сдвига
\begin{equation}
	\Gamma = \sqrt{3} ~ \varepsilon_{i}
\end{equation}