# Финишные покрытия ПП

- OSP (organic solderability preservative) - консервация (спиртовой раствор канифоли в ссср) - испаряется после первого нагрева, следовательно, надо проектировать линию так, чтобы за один раз пропаять все компоненты;
- NiAu (ENIG - electroless Ni & Immersion Gold - химический никель и иммерсионное золото)
- ImmSn (immersion Sn)
- HALS (hot-air solder leveling)
