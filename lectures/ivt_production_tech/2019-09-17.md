# Материалы ПП

## Критерии выбора материалов

- температура перехода ($T_g$);
- термическое расширение (КТР);
- прочность сцепления фольги с диэлектриком;
- диэлектрическая проницаемость;
- тангенс угла диэлектрических потерь;
- термостойкость;
- размерная стабильность;
- водопоглощение.

### Температура перехода $T_g$

Температурный коэффициент меди Cu - \num{17e-6}.
Температурный коэффициент диэлектрика - \num{300e-6}.
удлиннение диэлектрика $\Delta L / L$.
$T_g$ - температура стеклования (фазового перехода)

### тангенс

потери мощности в конденсаторе (линии передач) пропорциональны ...
подробнее смотреть [ТУТ](http://electricalschool.info/main/naladka/666-tangens-ugla-dijelektricheskikh-poter.html).


## Типы материалов

материалы для пп представляют собой полимер, армированный чем-либо.

- связующие (смолы)
    + epoxy (FR4) - эпоксидка;
    + polyimide - полиэмиды;
    + cyanate ester - полиэфиры;
    + BT epoxy - модифицированная эпоксидная смола;
    + PTFE - политетрофторэтилен, фторопласт, тефлон.
- пленкообразующие (термопленки - thermount) для создания ГПП
    + epoxy
    + polyimide

_FR4_ (fire resistivity, 4 - стеклоэпоксид)

- большое распространение
- высокая технологичность

_polyimide_

- высокая температура перехода

_цианатные полиэфиры_

- высокая $T_g$
- низкая диэлектричекая проницаемость
- низкие потери (фактор потерь - ФП)
- слабо исопльзуются

_модифицированные смолы_

- повышенная диэлектричекая проницаемость

_фторопласт_

- низкая дп
- низкий фп
- низкий ктр
- инертен к веществам -> сложно приклеивать фольгу, нужно активировать поверхность плазменной обработкой или агрессивной химией (при этом его свойства деградируют)
- ипользуется в высокочастотных трактах

## Управление импедансом

Диэлектричекая проницательность относительно слабо сказывается на волновом сопротивлении по сравнению с геометричесмими параметрами линии связи.
Желатлеьно выбирать ширину линий больше, чтобы меньше сказывалось подтравливание проводников.
Желательно также иметь бОльшую толщину межслойной изоляции, чтобы меньше сказывался эффект усадки при прессовании.

## Директивы RoHS

Технологии бессвинцовой пайки требуют использования материалов с высокой $T_g$.
Polyclad PCL-370HR хорошо ведет себя при высоких температурах.
Требуются материалы, не содержащие галогенов.

## Материалы теплоотводящих слоев

- традиционно для этого используется _медь-инвар-медь_;
- менее распространен _медь-молибден-медь_;
- новые фольгированные материалы и препреги обладают относительно высокой теплопроводностью (IMS-boards);
- медь остается самым лучшим теплопроводящим материалом.

## Температура деструкции

- температура деструкции $T_d$ определяется???
