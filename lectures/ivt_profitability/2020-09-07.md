# Проектное управление и экономическое обоснование

Речь идет о чем-то похожем на сценарий, рассказанном на недавнем вебинаре.
Из существующей иерархии организации выцепляются конечные исполнители, назначаются руководители, и из них формируются команды, отвечающие за конкретный "проект".
В советах эти команды назывались "трудовыми коллективами".

# Смета затрат на производство и реализацию разработки?

Смета затрат – все те затраты, которые несутся рпи реализации проекта (или при изготовлении изделия).

Смета затрат состоит из пяти экономических элементов, они определены постановлением правительством ~от 96 года:

1. материальные затраты с обычным НДС и возвратом 
   1. прямые
   2. затраты на электроэнергию
   3. затраты на отопление
   4. затраты на связь
2. фонд оплаты труда (ФОТ)
   1. основная заработная плата (ОЗП)
   2. дополнительная заработная плата (ДЗП) – (ну лол) за вовремя и качественно выполненную работу, не должна превышать 25% от ОЗП
3. страховые взносы ~30.2% от ФОТ
4. амортизационные отчисления – с материальных и нематериальных объектов
5. прочие затраты

Из этих 5 пунктов складывается итог по смете.

------

НДС – налог на добавочную стоимость

- Цена (с НДС) = 120% (100% + 20% [^1])

  [^1]: с 2020 года

- Стоимость (без НДС) = 100%

- Стоимость = Цена $\times \frac{100\%}{120\%}$

Цена – денежное выражение стоимости одной штуки продукции.

# Механизм ценообразования

Затратный метод ценообразования:
$$
Ц_р = С_{полн} + П_{план} + НДС + Акциз + ТН
$$
- Себестоимость:
  - технологическая
  - цеховая
  - производственная
  - полная – $С_{полн} = С_{техн} + С_{цеховая} + С_{произв}$
- $П_{план}$ – плановая прибыль от 9% до 25%
- НДС - 20%
- Акциз – до 10%
- Торговая наценка (надбавка) – от 0.5% до 25%

