# Деформация позвоночника

Позвоночник имеет следующие отделы:

- шейный - 7 позвонков;
- грудной - 12 позвонков;
- поясничный - 5 позвонков;
- крестцовый отдел - примерно 5 позвонков;
- копчик - 3-5 позвонков.

Деформации встречаются как во фронтальной плоскости, так и в сагиттальной. В сагиттальной плоскости углом, открытым вперед -- _кифоз_, открытым назад -- _лордоз_. Во фронтальной плоскости -- _сколиоз_. В последнем случае особо значение имеют ротационные смещения позвонков, что усугубляет характер самой деформации и ее клинические проявления. Легкие степени деформации позвонков лечатся консервативно, в лечение включено функциональное лечение и ограничение нагрузок. Наиболее эффективным является плавание и упражнения лежа, что исключает осевую (вертикальную) нагрузку на позвоночник. 

Ношение корсета предупреждает появление (развитие) деформации, однако нельзя забывать о том, что основным корсетом позвоночника является тонус мышц туловища, который разгружает позвоночник. Способ развития тонуса мышц -- плавание и упражнения лежа (who could've guessed). 

Другими заболеваниями позвоночника можно считать остеохондроз, когда происходят дегенеративные дистрофические изменения суставов позвонков по типу артроза суставов; грыжи; протрузии дисков. Все это нарушает кровоснабжение спинного мозга и его корешков или сдавливает их.

К оперативному лечению относится использование (при сколиотических деформациях) металлических конструкций, которые выпрямляют позвонки и удерживают их близко к правильной соосности. Также операции, часто малотравматичные, производятся при протрузиях и грыжах позвоночника.

# Деформация голеностопного сустава

К деформациям голеностопного сустава относятся, как правило, посттравматические повреждения, когда нарушено взаимоотношение костных элементов берцовых костей и таранной кости. Ввиду нарушения анатомической формы голеностопного сустава, нарушается его биомеханика, разрушается суставной хрящ, развивается артроз. 

Оперативное лечение включает три варианта:

1. Реконструктивные операции с целью восстановления анатомии. Этот вид оперативного вмешательства может быть эффективным на ранних этапах -- до 6-7 недель после травмы; 
2. На более поздних сроках восстановление анатомии будет неэффективно, в связи с тем, что имеются глубокие разрушения суставного хряща. В этом случае выполняется _артродез_ (артродезирование) -- полное выключение сустава соединением составляющих его костей -- вырезается суставной хрящ, создается контакт с сочленяющимися костями и достигается _костный анкилоз_ (сращение двух соседних костей).
3. Эндопротезирование голеностопного сустава -- это направление относительно молодое и по долговечности может уступать артродезу. 

# Деформации, стоп!

## Деформации переднего отдела стопы

Стопа имеет три отдела: задний, средний и передний. Часто встречаются деформации-плоскостопии, как продольные, так и поперечные. Стопа имеет два продольных свода и один поперечный. Первый продольный составляют по медиальной (внутренней) поверхности стопы пяточная кость, таранная, ладьевидная, клиновидная и первая плюсневая; второй продольный (по наружной) -- пяточная, клиновидная и 5-я плюсневая; поперечный свод составляют головки пяти плюсневых костей по дугообразной линии. Своды составляют рессорную функцию стопы.

При продольной плоскостопии уменьшается высота внутреннего свода. Поперечная плоскостопия проявляется как поперечная распластанность переднего отдела. Одним из заболеваний является _Hallux Valgus_ -- вальгусная деформация первого пальца. При этом первая плюсневая кость отклоняется к внутренней поверхности, а первый палец -- к наружи. Такое взаимоотнощение в связи с направленной тягой разгибателя первого пальца развивает угол, открытый к наружи, между продольной осью первой плюсневой кости и первого пальца. Эта деформация сочетается изменением формы 2,3,4 пальцев в виде молоткообразной деформации. Пальцы приобретают в плюсно-фаланговом суставе разгибательное положение, в первом межфаланговом суставе (между основной и средней фалангами) -- сгибательное положение. 

Вышесказанное относится к переднему отделу стопы. Лечение _Hallux Valgus_ -- оперативное. Консервативно применяемые иммобилизационные средства могут лишь остановить развитие заболевания. Оперативное лечение делится на два варианта: операции на сухожильно-мышечном аппарате; костные операции. Первые мало применяются и их эффективность низкая. Второй вариант преследует цель устранения образованного угла между первой плюсневой костью и пальцем, при этом осуществляют сближение _головки первой плюсневой кости_ к _головке второй плюсневой кости_, чем добиваются устранения распластанности. Этого можно достичь путем остеотомии плюсневой кости на одном (проксимальном или дистальном) уровне или двойной остеотомией. Фиксацию достигнутого положения диафиксирующими спицами, пластинами, малогабаритным аппаратом внешней фиксации. Фиксация длится 40-60 дней, требуется ношение специальной обуви для разгрузки передней части стопы. В целом восстановление передней части стопы может составлять до 6 месяцев. В некоторых случаях деформация устраняется добавлением в объем операции клиновидной резекции основной фаланги (клин основанием кнутри) и фиксацией нового положения винтами.

При молоткообразных деформациях производятся укорачивающие операции за счет резекции головки основной фаланги и трансартикулярной фиксации спицами на 30-40 дней.

## Деформации среднего отдела стопы

Составляющие кости: ладьевидная, кубовидная, три клиновидные. Часто возникают от переломов клиновидной или ладьевидной кости ввиду направления разгибателей, находящихся на латеральной стороне голени, возникает вальгусная деформация. Оперативное вмешательство -- остеотомия среднего отдела через суставы и кости, мобилизация переднего отдела стопы, установка в правильном анатомическом состоянии с фиксацией аппаратом внешней фиксации -- является наиболее эффективным, т.к. сопротивляемые силы имеют большую тенденцию к возникновению рецидива вальгуса. 

К таким деформациям также можно отнести деформации в суставе Лисфранка, который составляют три клиновидные, кубовидная и пять плюсневых костей. Такие повреждения также требуют проведения реконструктивных операций с клиновидной резекцией (основанием кнутри) с фиксацией спицами или винтами или без резекции аппаратами внешней фиксации.

## Деформации заднего отдела стопы

Задний отдел стопы состоит из пяточной и таранной костей. Взаимоотношенияе таранной и берцовых костей можно отнести к деформациям голеностопного сустава. Нарушение взаимоотношения таранной кости с пяточной или с ладьевидной костями -- к деформациям заднего отдела стопы. Происходит "стояние пятки". Углом к наружи -- вальгус пятки, пронация, углом кнутри -- супинация или варус пятки. 

Реконструкция производится артродезированием таранно-пяточного сустава. Однако это может привести к состоянию "минус ткань" и уплощению стопы. В этом случае целесообразно использовать аппарат внешней фиксации с остеотомией пяточной кости образованием продольного свода.

# Контрактура суставов

Это ограничение траектории движения костей, образующих данный сустав. Наиболее наглядно это в коленном и локтевом суставах. Наличие контрактуры вызывает серьезные нарушения функций как нижних, так и верхних конечностей. Различают разгибательную контрактуру, когда (на примере коленного сустава) голень не сгибается; сгибательная контрактура -- нога согнута и не разгибается. Также существует сгибательно-разгибательная контрактура -- не туда и не сюда. 

Лечение зависит от стадии и давности контрактур, вовлечения элементов мышц, суставной капсулы, конкурентности суставных поверхностей костей. При легких степенях можно установить на ближайшие к суставу сегменты аппараты внешней фиксации, обеспечить их шарнирными узлами и увеличить объем движения. При застарелых случаях, когда контрактура имеет место не один год (до 10 лет), при наличии качательных движений (свидетельствует об отсутствии анкилоза), можно делать (для расслабления мышц) укорачивающие операции.

При нарушении костных элементов -- полное разрушение хряща -- выполняют артродезирование сустава, особенно для нижних конечностей, или эндопротезирование, что важно для коленного или локтевого сустава.

# Остеомиелит

Остеомиелит -- гнойно-воспалительное заболевание кости, всех элементов -- надкостницы, переоста (переостит), самой кости и костного мозга (миелит). Различают остеомиелиты как приобритенные (часто после открытых переломов, осложненных оперативных вмешательств) и как заболевание, например, 1-ый хронический остеомиелит. Последний часто встречается у детей, трактуется как гематогенный остеомиелит. В анамнезе -- упал, ушиб, температура, готово. В некоторых случаях на этом этапе эффективно проведение комплексного противовоспалительного лечения -- антибиотики и витамины.

Остеомиелит сопровождается разрушением костной ткани, образованием "секвестеров" (в обществе, коллективе -- умерший человек, которого надо удалять) -- нежизнеспособной костной ткани, которая поддерживает гнойно-воспалительный процесс и требует удаления. 

Особую группу составляет остеомиелит, который развился после перелома, где для фиксации отломков были применены внутренние фиксирующие имплантаты. Как правило, эти имплантаты нестабильно фиксируют, разбалтываются, не достигается сращение переломов. При этом сталкиваемся с тремя проблемами:

1. наличие нефиксирующей металлоконструкции
2. несращение переломов
3. гнойно-воспалительный процесс -- остеомиелит

Задача -- удалить конструкцию, провести санацию, создать фиксацию наружную и добиваться сращения. Санация с удалением секвестора приводит к дефектам. Дефект устраняется путем остеотомии вдали от очага кости перемещением среднего фрагмента вниз с достижением билокального остеосинтеза по Илизарову.

<!-- не удоляй меня -->