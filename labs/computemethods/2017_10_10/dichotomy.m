function [x] = dichotomy(f, a, b, eps)

	x = (a + b) / 2;

    while (b - a >= eps)
        f_1 = f(a);
        f_2 = f(x);
		
		if (f_1 * f_2 < 0)
			b = x;
        else
            a = x;
		end	
		
		x = (a + b) / 2;
    end	

end