function [x_1] = newtons(f, df, ddf, a, b, eps)
    x_0 = a;
    if (f(x_0) * ddf(x_0) > 0)
        x_1 = x_0 - f(x_0) / df(x_0); 
        while (abs(x_0 - x_1) > eps)
            x_0 = x_1; 
            x_1 = x_0 - f(x_0) / df(x_0);
        end 
    end
    x_0 = b;
    if (f(x_0) * df(x_0) > 0)
        x_1 = x_0 - f(x_0) / df(x_0); 
        while (abs(x_0 - x_1) > eps)
            x_0 = x_1; 
            x_1 = x_0 - f(x_0) / df(x_0);
        end 
    end
end