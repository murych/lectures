%% # 1
h = 0.02;
x = 0.02 : h : 0.12;
y = [1.02316 1.09590 1.14725 1.21483 1.30120 1.40976];
x_1 = 0.11;

RsN_ = newton(x, y, x_1, h);
RsL_ = lagrange(x, y, x_1);

for i=1:length(x)
    RsL(i) = lagrange(x, y, x(i));
end

for i=1:length(x)
    RsN(i) = newton(x, y, x(i), h);
end

plt = plot(x, y, '-r', [x x_1], [RsN RsN_], 'b-+', [x x_1], [RsL, RsL_], 'g-o')
set(plt(1), 'linewidth', 3)
legend('function', 'newton', 'lagrange')

%% #2
%clearvars;
h = 0.2;
x = 0 : h : 1;
y = [1.2715 2.4652 3.6443 4.8095 5.9614 7.1005];

r_1 = 0.1;
r_2 = 0.9;

RsL_x_1 = lagrange(x, y, r_1);
RsN_x_1 = newton(x, y, r_1, h);

RsL_x_2 = lagrange(x, y, r_2);
RsN_x_2 = newton(x, y, r_2, h);

for i=1:length(x)
    RsL(i) = lagrange(x, y, x(i));
end

for i=1:length(x)
    RsN(i) = newton(x, y, x(i), h);
end


%plot(x, y, '-r', x(:,2:6), RsN(:,2:6), 'bo', x, RsL, '+g')
plot(x, y, '-r', x, RsN, 'bo', x, RsL, '+g')
legend('table data', 'newton', 'lagrange')