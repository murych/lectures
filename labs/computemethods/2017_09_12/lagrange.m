function S = lagrange(x, y, r)

    n = length(x);
    S = 0;

    for i = 1:n
        P = 1;
        for j = 1:n
            if(i ~= j)
                P = P * (  r - x(j) ) / ( x(i) - x(j)  );
            end
        end
        S = S + y(i) * P;
    end

end