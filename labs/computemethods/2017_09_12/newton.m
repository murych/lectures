function S = newton( x, y, r, h )

P = 1;
n = length(x);

if r >= x(1) && r <= x(round(length(x)/2))
    S = y(1);
    t = ( r - x(1) ) / h;
    for i = 1 : n - 1
        delta = round(diff(y, i), 4);
        delta = delta(1);
        for j = 1 : i
            P = P * ( t - j + 1 );
        end
        S = S + ( delta * P / factorial(i) );
    end
else
    S = y(n);
    t = ( r - x(n) ) / h;
    for i = 1 : n - 1
        delta = round(diff(y, i), 4);
        delta = delta(length(delta));
        for j = 1 : i
            P = P * ( t + j - 1 );
        end
        S = S + ( delta * P / factorial(i) );
    end
end

end