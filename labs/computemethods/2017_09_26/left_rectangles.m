function [ Ih ] = left_rectangles( a, b, n )

h = ( b - a ) / n;
Ih = 0;

for i = 0 : 1 : (n - 1)
    x = a + i * h;
    F = integral_f(x);
    Ih = Ih + F * h;
end

end