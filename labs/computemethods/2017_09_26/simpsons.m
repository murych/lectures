function [ Ih ] = simpsons( a, b, n )

h = ( b - a ) / n;

Ih = integral_f(a) + integral_f(b);

for i = 2 : 2 : n
    x = a + i * h;
    Ih = Ih + 2 * integral_f(x);
end

for i = 1 : 2 : (n-1)
    x = a + (i-1) * h;
    Ih = Ih + 4 * integral_f(x);
end

Ih = h * Ih / 3;

end