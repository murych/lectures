%%
a = 0; b = pi/4; eps = 0.001;
%% Left rectangles
Ih1 = left_rectangles(a, b, 1);
Ih2 = left_rectangles(a, b, 2);

p = 1;
n = 1;

R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );

while (R > eps)
  Ih1 = Ih2;
  n = n + 1;
  Ih2 = left_rectangles(a, b, 2^n);
  R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );
end;

display('Method: Left rectangles')
display('Iterations')
n  
display('Result')
Ih2
%% Average rectangles
Ih1 = average(a, b, 1);
Ih2 = average(a, b, 2);

p = 2;
n = 1;

R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );

while (R > eps)
  Ih1 = Ih2;
  n = n + 1;
  Ih2 = average(a, b, 2^n);
  R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );
end;

display('Method: Average rectangles')
display('Iterations')
n  
display('Result')
Ih2
%% Trapeze
Ih1 = trapeze(a, b, 1);
Ih2 = trapeze(a, b, 2);

p = 2;
n = 1;

R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );

while (R > eps)
  Ih1 = Ih2;
  n = n + 1;
  Ih2 = trapeze(a, b, 2^n);
  R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );
end;

display('Method: Trapeeze')
display('Iterations')
n  
display('Result')
Ih2
%% Simpson
Ih1 = simpsons(a, b, 1);
Ih2 = simpsons(a, b, 2);

p = 4;
n = 1;

R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );

while (R > eps)
  Ih1 = Ih2;
  n = n + 1;
  Ih2 = simpsons(a, b, 2^n);
  R = abs( ( Ih1 - Ih2 ) / ( 2^p - 1 ) );
end;

display('Method: Simpson')
display('Iterations')
n  
display('Result')
Ih2