function [ Ih ] = average( a, b, n )

h = ( b - a ) / n;
Ih = 0;

for i = 1 : (n - 1)
  x = (( 2 * a + (i * h) + ((i - 1) * h) ) / 2);
  F = integral_f(x);
  Ih = Ih + ( F * h );
end

end