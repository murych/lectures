program Roman_3;

uses
    crt;

const
    nmax = 30;

type
    entry = record
        surname: String;
        name: String;
        sex: String;
        mark: String;
    end;
    
    mass = array[1..nmax] of entry;

var
    entries: mass;
    group: String;
    menu: integer;

function MainMenu: integer;
var
    menu: integer;
begin
    writeln('1 - Прочитать базу данных из файла');
    writeln('2 - Заполнить базу данных');
    writeln('3 - Вывести базу данных на экран');
    writeln('4 - Дополнить базу данных');
    writeln('5 - Девушек в студию');
    writeln('0 - Выход');
    readln(menu);
    MainMenu := menu;
end;

procedure read_from_file(var entries: mass);
var
    file_in: text;
    i: integer;
begin
    assign(file_in, 'input.txt');
    reset(file_in);
    i := 1;
    while not eof(file_in) do
    begin
        with entries[i]	do
            with entry do 
            begin
                readln(file_in, surname);
                readln(file_in, name);
                readln(file_in, sex);
                readln(file_in, mark);
            end;
        i := i + 1;
    end;
    close(file_in);
end;

procedure screen_output(var entries: mass);
var
    i: Integer;
begin
    clrscr;
    writeln('Список группы ', group);
    writeln('|      Фамилия      |   Имя   |  Пол  | Средний балл |');
    for i := 1 to nmax do
    begin
        with entries[i] do
            with entry do
                if not (surname = ('')) then
                    writeln('| ', surname:18, '|', name:9, '|', sex:7, '|', mark:14, '|');
    end;
    readln();
end;

procedure enter(var entries: mass);
var
    i, n: integer;
begin
    clrscr;
    write('Введите количество студентов <= 20: ');
    readln(n);
    for i := 1 to n do
    begin
        writeln('Введите информацию о ', i, ' студенте');
        write('Фамилия: ');
        readln(entries[i].surname);
        write('Имя: ');
        readln(entries[i].name);
        write('Пол (m|f): ');
        readln(entries[i].sex);
        write('Средний балл за сессию: ');
        readln(entries[i].mark);
    end;
end;

procedure add_entry(var entries: mass);
var
    i: integer;
begin
    clrscr;
    for i := 1 to nmax do 
    begin
        if entries[i].surname = '' then
        begin
            writeln('Добавление нового студента');
            write('Фамилия: ');
            readln(entries[i].surname);
            write('Имя: ');
            readln(entries[i].name);
            write('Пол (m|f): ');
            readln(entries[i].sex);
            write('Средний балл за сессию: ');
            readln(entries[i].mark);
            break;
        end;
    end;
end;

procedure sort(var entries: mass);
var
    i, j, n: integer;
begin
    for n := 1 to nmax do
        if entries[n].surname = '' then break;
    for j := 1 to n - 1 do
        for i := 1 to n - j do
            if entries[i].mark > entries[i + 1].mark then
                swap(entries[i], entries[i + 1]);
end;

procedure girls(var entries: mass);
var
    i: integer;
begin
    clrscr;
    sort(entries);
    writeln('Девушки в порядке увеличения оценки');
    writeln('|      Фамилия      |   Имя   |  Пол  | Средний балл |');
    for i := 1 to nmax do
    begin
        with entries[i] do
            with entry do
                if sex = 'f' then
                    writeln('| ', surname:18, '|', name:9, '|', sex:7, '|', mark:14, '|');
    end;
    readln();
end;

BEGIN
    repeat
        clrscr;
        menu := MainMenu;
        case menu of
            1: read_from_file(entries);
            2: enter(entries);
            3: screen_output(entries);
            4: add_entry(entries);
            5: girls(entries);
        end;
    until menu = 0;
END.