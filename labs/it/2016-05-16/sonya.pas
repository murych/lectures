program Timur_3;

uses
	crt;

const
	nmax = 30;

type
	entry = record
		surname: string;
		name: string;
		patronymic: string;
		phone: string;
	end;
	
	mass = array[1..nmax] of entry;

var
	entries: mass;
	menu: integer;

function MainMenu: integer;
var menu: integer;
begin
	writeln('1 - Заполнить базу данных из файла');
	writeln('2 - Вывести базу данных на экран');
	writeln('3 - Поиск по ФИО');
	writeln('4 - Поиск по номеру');
	writeln('0 - Выход');
	readln(menu);
	MainMenu := menu;
end;

procedure read_from_file(var entries: mass);
var file_in: text;
	i: integer;
begin
	assign(file_in, 'input.txt');
	reset(file_in);
	i := 1;
	while not eof(file_in) do
	begin
		with entries[i] do
			with entry do 
			begin
				readln(file_in, surname);
				readln(file_in, name);
				readln(file_in, patronymic);
				readln(file_in, phone);
			end;
		i := i + 1;
	end;
	close(file_in);
end;

procedure screen_output(var entries: mass);
var i: integer;
begin
	clrscr;
	writeln('Список группы');
	writeln('|      Фамилия      |     Имя    |    Отчество    |      Номер      |');
	for i := 1 to nmax do
	begin
		with entries[i] do
			with entry do
				if not (surname = ('')) then
					writeln('| ', surname:18, '|', name:12, '|', patronymic:16, '|', phone:17, '|');
	end;
	readln();
end;

procedure search_by_name(var entries: mass);
var name_: string;
	i, j, n: integer;
begin
	clrscr;
	for n := 1 to nmax do if entries[n].surname = '' then break;
	writeln('Введите ФИО студента');
	readln(name_);

	j := 0;
	for i := 1 to n do
		with entries[i] do
			with entry do
				if surname + ' ' + name + ' ' + patronymic <> name_ then j := j + 1
				else continue;
	if j = n then writeln('Студент ', name_, ' не найден');

	for i := 1 to n do
		with entries[i] do
			with entry do
				if surname + ' ' + name + ' ' + patronymic = name_ then begin
					writeln('Найден студент ', name_);
					writeln('Его номер: ', phone);
				end;
	readln();
end;

procedure search_by_phone(var entries: mass);
var phone_: string;
	i, j, n: integer;
begin
	clrscr;
	for n := 1 to nmax do if entries[n].surname = '' then break;
	writeln('Введите номер студента');
	readln(phone_);

	j := 0;
	for i := 1 to n do
		with entries[i] do
			with entry do
				if phone <> phone_ then j := j + 1
				else continue;
	if j = n then writeln('Студент с номером ', phone_, ' не найден');

	for i := 1 to n do
		with entries[i] do
			with entry do
				if phone = phone_ then begin
					writeln('Найден студент с номером ', phone_);
					writeln('Его ФИО: ', entries[i].surname, ' ', entries[i].name, ' ', entries[i].patronymic);
				end;
	readln();
end;

BEGIN
	repeat
		clrscr;
		menu := MainMenu;
		case menu of
			1: read_from_file(entries);
			2: screen_output(entries);
			3: search_by_name(entries);
			4: search_by_phone(entries);
		end;
	until menu = 0;
END.