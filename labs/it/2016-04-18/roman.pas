uses crt;

var
  j, i: integer;
  s, w: string;

BEGIN
  j := 1;
  readln(s);
  
  for i := 1 to length(s) do 
  begin
    if s[i] = ' ' then begin
      w := copy(s, j, i - j);
      w[1] := '#';
      j := j + length(w)+1;
      write(w, ' ');
    end;    
  end;
  readln;
END.