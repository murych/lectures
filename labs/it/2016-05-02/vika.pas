// z(3t+1, -2s, 1.8t) + z(2.6, s, 3t+s)
//               |a|+|b|+|c|
// z(a,b,c) = -----------------
//             a^2 + b^3 + c^4

uses crt;

var s, t: real;

function z(a, b, c: real): real; begin
z := (abs(a)+abs(b)+abs(c))/(a*a + b*b*b + c*c*c*c)
end;
BEGIN
readln(s, t);
writeln(z(3*t+1, -2*s, 1.8*t) + z(2.6, s, 3*t+s));
END.