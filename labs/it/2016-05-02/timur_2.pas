// z = (sign(x) + sign(y) * sign(x + y))
//          -1, a < 0 
// sign(a) = 0, a = 0
//           1, a > 0

var a, b, c, x, y, z: integer;
procedure sign(x, y: integer; var result: integer);
begin
    if x + y < 0 then result := -1;
    if x + y = 0 then result := 0;
    if x + y > 0 then result := 1;
end;

BEGIN
    readln(x, y);
    sign(x, 0, a);
    sign(0, y, b);
    sign(x, y, c);
    writeln(a + b * c);
END.