program Roman_2;
uses crt;

var
  s, t: real;

function y(a, b: real): real;
begin
  y := (Sqr(a) / (1 + b)) + (Sqr(b) / (1 + a)) - Sqr(a + b)
end;

BEGIN
  readln(s, t);
  writeln(y(s, t) + Sqr(y(t - s, s * t)));
END.