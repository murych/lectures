// z = (sign(x) + sign(y) * sign(x + y))
//          -1, a < 0 
// sign(a) = 0, a = 0
//           1, a > 0

uses crt;

var
  x, y, z: integer;

function sign(a: integer): integer;
begin
  if a < 0 then sign := -1;
  if a = 0 then sign := 0;
  if a > 0 then sign := 1;
end;

BEGIN
  readln(x, y);
  z := sign(x) + sign(y) * sign(x + y);
  writeln(z);
END.